<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

 
if ($this->setting [ 'debug' ] == "yes" )Gsm_debug ( array ( "field" => $this->page_content) , __LINE__ . __FUNCTION__ . " print start" );

$pdf = false;
if ( isset ( $this->setting [ 'pdf_filename' ] ) && strlen ( $this->setting [ 'pdf_filename' ] ) > 10 ) {
	$pdf = true;
	require_once ( $this->setting [ 'includes' ] . 'classes/pdf.inc' );
	$pdf = new PDF();
	global $owner;
	$owner = $this->setting [ 'owner' ];
	global $title;
	$title = ""; //$project;
	$pdf->AliasNbPages();
	$z=1;
	$pdf->AddPage();
	$pdf_text   = '';
	$ral = array();
	$pdf_cols = array( 6, 68, 32, 22, 6, 6); 
	$pdf_text .= "Berekening Transitievergoeding (ex artikel 7:673 BW)" . "\n"; 
	$pdf->ChapterXLarge ( $pdf_text );
	$pdf_text   = '';

//	$pdf->ChapterBody( $pdf_text );
//	$pdf->ChapterTitle ( $z, "titel part 1" );
//	$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", "54", "34", '35', "1",  "20", "20" ) ) );
//	$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", "123456789", "123456789", '123456789', "123456789",  "987654321", "987654321" ) ) );
//	$pdf->DataTable( $pdf_header, $ral, $pdf_cols );
//	$pdf->DataKolom( $ral, $pdf_cols );
//	$ral = array (); 
//	$pdf_text .= "\n";
//	$pdf->ChapterBody( $pdf_text );
//	$pdf_text = "";
}

$Bruto_maandsalaris = $this->gsm_sanitizeStringv ( $this->page_content [ 'b030' ] ?? "0", "v{". $this->page_content[ 'c002' ].";0;20000}" );
$Bruto_maandsalarisE = $this->gsm_sanitizeStrings ( $Bruto_maandsalaris, "s{ FULL|E128 }" );

if ($pdf ) {
	if ( strlen ( $this->page_content [ 'b001' ] ) >3 ) {
		$pdf_text .= "\n".'Naam werknemer : '; 
		$pdf_text .= $this->page_content [ 'b001' ];
 		$pdf_text .= "\n";
		$pdf->ChapterLarge( $pdf_text );
		$pdf_text = "";
	}
}

if ($pdf ) {
	if ( strlen ( $this->page_content [ 'b010' ] ) >3 ) {
		$pdf_text .= "\n".'Naam werkgever : ';
		$pdf_text .= $this->page_content [ 'b010' ];
		$pdf_text .= "\n";
		$pdf->ChapterLarge( $pdf_text );
		$pdf_text = "";
	}
}
if ($pdf ) {
	$pdf_text .= "\n".'Gegevens werknemer : ';
	$pdf_text .= "\n\n";
	$pdf->ChapterLarge( $pdf_text );
	$pdf_text = "";
	if ( $this->page_content [ 'b002' ] != $this->language [ 'trans' ][ 'b002' ] ) 
		$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Geboortedatum : ", "",$this->gsm_sanitizeStrings ( $this->page_content [ 'b002' ], "s{ DATUM }" ),   "", "" ) ) );
	$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Datum in dienst : ", "", $this->gsm_sanitizeStrings ( $this->page_content [ 'b020' ], "s{ DATUM }" ), "", "" ) ) );
	$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Datum uit dienst : ", "", $this->gsm_sanitizeStrings ( $this->page_content [ 'b021' ], "s{ DATUM }" ), "", "" ) ) );		
	if ( $this->page_content [ 'b002' ] != $this->language [ 'trans' ][ 'b002' ] ) 	{
		if ( date ( "Y-m-d", strtotime( '-1 year', strtotime ( $this->page_content [ 'c023' ] ) ) ) < $this->page_content [ 'b021' ]) {
			$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Pensioendatum : ", "",$this->gsm_sanitizeStrings ( $this->page_content [ 'c023' ], "s{ DATUM }" ),  "", "" ) ) );		
		}
	}	
	$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", "", "", "", "", "", "" ) ) );
	$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", "", "Bruto maandinkomen *: ", "", $Bruto_maandsalarisE, "", "" ) ) );
	$pdf->DataKolom( $ral, $pdf_cols );
	$pdf_text .= '* Dit maandinkomen is inclusief vakantietoeslag en 1/12 van de eventuele bonus, provisie en 13e/14e maand';
	$pdf_text .= "\n\n";
	$pdf->ChapterKlein( $pdf_text );
	$ral = array ();
	$pdf_text = "";
}

if ($pdf ) {
	$pdf_text .= "\n".'Duur dienstverband'. "\n\n";
	$pdf->ChapterLarge( $pdf_text );
	$pdf_text = "";
	$Lhulp00 = sprintf ( "%s jaren, %s maand(en) en %s dag(en)" ,
		$this->page_content [ 'c020' ], 
		$this->page_content [ 'c021' ], 
		$this->page_content [ 'c022' ] );
	$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", $Lhulp00, "", "", "", "" ) ) );
	$pdf->DataKolom( $ral, $pdf_cols );
	$ral = array ();
}
if ($pdf ) {
	$pdf_text  .= "\n".'Berekening transitievergoeding'. "\n\n";
	$this->page_content [ 'c025' ] = 0;
	$pdf->ChapterLarge( $pdf_text );
	$pdf_text = "";
	if ($this->page_content [ 'c020' ] >0 ) {
		if ( $this->page_content [ 'c020' ] == 1 ) { 
			$Lhulp00 = sprintf ( "%s jaar x %s x 1/3" , $this->page_content [ 'c020' ], $Bruto_maandsalarisE );
		} else {
			$Lhulp00 = sprintf ( "%s jaren x %s x 1/3" , $this->page_content [ 'c020' ], $Bruto_maandsalarisE );
		}
		$Lhulp01 = round ( $Bruto_maandsalaris * $this->page_content [ 'c020' ] / 3, 2 );
		$this->page_content [ 'c025' ] = $this->page_content [ 'c025' ] + $Lhulp01;
		$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", $Lhulp00, "", $this->gsm_sanitizeStrings ( $Lhulp01, "s{ FULL|E128 }" ), "", "" ) ) );
	}
	if ($this->page_content [ 'c021' ] >0 ) {
		if ( $this->page_content [ 'c021' ] == 1 ) { 
			$Lhulp00 = sprintf ( "%s maand x %s x 1/3 x 1/12" , $this->page_content [ 'c021' ], $Bruto_maandsalarisE );
		} else{
			$Lhulp00 = sprintf ( "%s maanden x %s x 1/3 x 1/12" , $this->page_content [ 'c021' ], $Bruto_maandsalarisE );
		}
		$Lhulp01 = round ( $Bruto_maandsalaris * $this->page_content [ 'c021' ] / 36 , 2 );
		$this->page_content [ 'c025' ] = $this->page_content [ 'c025' ] + $Lhulp01;
		$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", $Lhulp00, "", $this->gsm_sanitizeStrings ( $Lhulp01, "s{ FULL|E128 }" ), "", "" ) ) );
	}
	if ( $this->page_content [ 'c022' ] >0 ) {
		if ( $this->page_content [ 'c022' ] == 1 ) { 
			$Lhulp00 = sprintf ( "%s dag x %s x 1/3 x 1/365" , $this->page_content [ 'c022' ], $Bruto_maandsalarisE );
		} else {
			$Lhulp00 = sprintf ( "%s dagen x %s x 1/3 x 1/365" , $this->page_content [ 'c022' ], $Bruto_maandsalarisE );
		}
		$Lhulp01 = round ( $Bruto_maandsalaris * $this->page_content [ 'c022' ] / 3 / 365 , 2 );
		$this->page_content [ 'c025' ] = $this->page_content [ 'c025' ] + $Lhulp01;
		$ral[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", $Lhulp00, "", $this->gsm_sanitizeStrings ( $Lhulp01, "s{ FULL|E128 }" ), "", "" ) ) );
	}
	$pdf->DataKolom( $ral, $pdf_cols );
	$ral = array ();
	$Lhulp1x =  $this->gsm_sanitizeStringv ( $this->page_content [ 'c001' ] ?? "0", "v{". $this->page_content[ 'c001' ].";0;100000}" );
	if ( $Lhulp1x < $this->page_content [ 'c025' ] ) {
		$this->page_content [ 'c025' ] = $Lhulp1x;
		$pdf_text .= 'De transitievergoeding is in ' . $this->page_content [ 'c003' ] . ' gemaximeerd op '.$this->gsm_sanitizeStrings ( $Lhulp1x, "s{ WHOLE|E128 }" ); 
		$pdf_text .= "\n".'Voor andere jaren is wellicht een ander maximum van toepassing';
		$pdf_text .= "\n";
		$pdf->ChapterBody( $pdf_text );
		$pdf_text = "";
	}
	if ( $this->page_content [ 'b002' ] != $this->language [ 'trans' ][ 'b002' ] ) {
		if ( date ( "Y-m-d", strtotime ( $this->page_content [ 'c023' ] ) ) < $this->page_content [ 'b021' ]) {
			$pdf_text .= 'Na pensioendatum is er geen transitievergoeding regeling';
			$this->page_content [ 'c025' ] = 0;
			$pdf_text .= "\n";
			$pdf->ChapterBody( $pdf_text );
			$pdf_text = "";
		}
	}
	if ( $this->page_content [ 'b021' ] < "2020-01-01" )	{
		$pdf_text .= 'Voor deze datum uit dienst is er een afwijkende regeling';
		$this->page_content [ 'c025' ] = 0;
		$pdf_text .= "\n";
		$pdf->ChapterBody( $pdf_text );
		$pdf_text = "";
	}		
}

if ($pdf ) {
	$pdf_text .= "\n\n".'Transitievergoeding : ' ;
	$pdf_text .= $this->gsm_sanitizeStrings ( $this->page_content [ 'c025' ], "s{ FULL|E128 }" );
	$pdf_text .= " Bruto "."\n";
	$pdf->ChapterXLarge( $pdf_text );
	$pdf_text = "";
}
/*
if ($pdf) {
	$pdf_cols = array( 97, 23, 20, 30, 15, 0); 
	$pdf_header = array( "", "Parameter", "Waarde", "" , "--", "" );
	$PageSwitch = true;
	if ($PageSwitch) {
		$z++;
		$pdf->ChapterTitle( $z, "Investerings aftrek" );
		$pdf->DataTable( $pdf_header, $ral, $pdf_cols );
		$pdf_text .= "\n";
		$pdf->ChapterBody( $pdf_text );
	}
	$pdf_text = "";
	$ral = array (); 
}
*/
// pdf afsluiting
if ($pdf) {
	$pdf_text .= 'Deze berekening van de transitievergoeding, gebaseerd op de ingevoerde gegevens, is gemaakt op : ' . $this->gsm_sanitizeStrings ( date ( "d M Y", time ( ) ), "s{ DATUM }" ) . "\n\n"; 
	$pdf_text .= "De berekening van de transitievergoeding is met zorgvuldigheid gemaakt." . "\n";
	$pdf_text .= "SMART Advocaten aanvaardt geen enkele aansprakelijkheid naar aanleiding van deze berekening." . "\n"; 
	$pdf_text .= "Heeft u meer vragen, neem dan gerust contact op met:" . "\n\n"; 
	$pdf_text .= "SMART Advocaten B.V." . "\n\n" . "Luchthavenweg 53 " . "\n" . "5657 EA Eindhoven" . "\n" . "tel: 040 2841172" . "\n" . "info@smartadvocaten.nl" . "\n" . "www.smartadvocaten.nl" . "\n"; 
	$pdf->ChapterFooter( $pdf_text );
}
