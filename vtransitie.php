<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/ 
$module_name = 'vtransitie';
$version = ' v20240210';
$project = "Berekening Transitie Vergoeding";
$proces_defaults_at = "transitie_json_defaults.html";
$default_template = '/transitie.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffc::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET , "droplet" );

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

$oFC->page_content [ 'DATEHIGH' ] = ( date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m" ) + 24, '01', date ( "Y" ) ) ) );
$oFC->page_content [ 'pdf_location' ] = sprintf ( "%s/%s/XX_%s", $oFC->setting [ 'collectdir' ], date ("Y_m_d"), session_id ( ) );
$oFC->page_content [ 'gebruik' ] = '';


/* get procesdefaults 1 */
$oFC->page_content = array_merge ( $oFC->page_content, $oFC->language [ 'trans'] );

/* get procesdefaults 2 * /
$LocalHulp = $oFC->setting [ 'frontend' ]. $proces_defaults_at;
$LocalHulp1 = file_get_contents ( $LocalHulp );	
$oFC->page_content = array_merge ( $oFC->page_content, json_decode ( $LocalHulp1 , true ) );

/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}
/* create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* get memory values * / 
$oFC->gsm_memorySaved ( );

/* get memory values */
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC, $selection ?? ""), __LINE__ . __FUNCTION__ ); 

/* selection * /
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}	

/* sips test before job * / 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
		$oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* reset before job */ 
if ( isset( $_POST[ 'command' ] ) && $_POST [ 'command' ] == "Reset" ) { 
	unset ($_POST); 
	$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
		$oFC->language [ 'TXT_REMOVE_INPUT' ] . NL; 
}

/*
 * which job to do
 */
if ( isset( $_POST[ 'command' ] ) ) {
	$oFC->page_content [ 'P1' ] = false; // niet eerste cycle 
	foreach ($_POST as $pay =>$load ) {	
		if (substr ( $pay, 0, 4 ) == "gsm_" ) {
			$oFC->page_content [ substr ( $pay, 4 ) ] = strip_tags ( $load );	
	}	}
	/* externe checks */ 
	$check = $oFC->setting [ 'includes' ] . $module_name . '_check' . '1' . '.php';
	if ( file_exists ( $check ) ) require_once ( $check ); 	
	
	/* jobs */ 
	switch ( $_POST[ 'command' ] ) {
		case "Reset":
			break;
		case "Print":
			$oFC->setting [ 'pdf_filename' ] = $oFC->gsm_sanitizeStrings ( sprintf ( "transitie_%s_%s.pdf", 
					date ( "Ymd", time ( ) ), 
					$oFC->page_content [ 'b001' ] ), 
					"s{FILE}" );
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {	
		default:
			// escape route 
			$oFC->page_content [ 'P1' ] = true; // eerste cycle aangenomen
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else {
	$oFC->page_content [ 'MODE' ] = 9;
	$oFC->page_content [ 'P1' ] = true; // eerste cycle 
	$oFC->gsm_existDir ( $oFC->page_content [ 'pdf_location' ] , true  );
}

// einde input verwerking
/* debug * / Gsm_debug (array ( "this"=>$oFC), __LINE__ . __FUNCTION__ ); /* debug */

/* opmaak voor de uitvoer */
if ( !$oFC->page_content [ 'P1' ] ) {
	$check = $oFC->setting [ 'includes' ] . $module_name . '_check' . '2' . $oFC->page_content [ 'gebruik' ] . '.php';
	if ( file_exists ( $check  ) ) require_once ( $check ); 
	// printuitvoer
	$oFC->setting [ 'pdf_filename' ] = $oFC->gsm_sanitizeStrings ( sprintf ( "transitie_%s_%s.pdf", date ( "Ymd", time ( ) ), $oFC->page_content [ 'b001' ] ), "s{FILE}" );
	if ( isset ( $oFC->setting [ 'pdf_filename' ] ) && strlen ( $oFC->setting [ 'pdf_filename' ] ) > 10 ) 
		$oFC->gsm_print ( "", 
			$project, "" , 
			"1" . $oFC->page_content [ 'gebruik' ], 
			$oFC->page_content [ 'pdf_location' ]  );
}

/*
 * the selection options
 */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	case 9:
	default: 
		if ( !$oFC->page_content [ 'P1' ] ) 
			$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 11 ), 
				"-", 
				"/" . $oFC->setting [ 'pdf_filename' ], 
				"-", "-", "-", "-", 
				$oFC->page_content [ 'pdf_location' ] );
		$oFC->page_content [ 'SELECTIOND' ] = $oFC->gsm_opmaakSel ( array ( 6, 7) );
		if ( $oFC->page_content [ 'b002' ] == $oFC->language [ 'trans' ][ 'b002' ] )	
			$oFC->page_content [ 'b002' ] = "";
		break; 
}

/*
 * the output to the screen
 */

if ( $oFC->page_content [ 'P1' ] ) { 
	$oFC->page_content [ 'REFERENCE_ACTIVE1'] = 'active'; 
	$oFC->page_content [ 'REFERENCE_ACTIVE2'] = ''; 
} else {
	$oFC->page_content [ 'REFERENCE_ACTIVE2'] = 'active'; 
	$oFC->page_content [ 'REFERENCE_ACTIVE1'] = ''; 
}

/* memory save * /
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( ); 

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) 
	$oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
$oFC->page_content [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if (LOAD_MODE == "x" )  
	$_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: //list
		break;
} 

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>