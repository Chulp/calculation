<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

//check values

if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content ), __LINE__ . __FUNCTION__ ); 
$oFC->page_content [ 'c001' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'c001' ] ?? "--", "s{ TOASC|CLEAN|TRIM }" );
$oFC->page_content [ 'c002' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'c002' ] ?? "--", "s{ TOASC|CLEAN|TRIM }" );
$oFC->page_content [ 'c003' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'c003' ] ?? "--", "s{ TOASC|CLEAN|TRIM }" );
$oFC->page_content [ 'c006' ] = $oFC->gsm_sanitizeStringD ( $oFC->page_content [ 'c006' ] ?? $oFC->language ['cal'][ 'c006' ], 
	"y{" . $oFC->language ['cal'][ 'c006' ] . ";" 
		. $oFC->language ['cal'][ 'c006' ] . ";" 
		. $oFC->page_content [ 'DATEHIGH' ] ."}" );
		
$oFC->page_content [ 'c010' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'c010' ] ?? "", "v{20;4;300}" );	
$oFC->page_content [ 'c011' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'c011' ] ?? "", "v{160;40;250}" );	

$oFC->page_content [ 'c015' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'c015' ] ?? "0", "v{0;0;200}" );	
$oFC->page_content [ 'c016' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'c016' ] ?? "0", "v{0;0;100}" );
$oFC->page_content [ 'c017' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'c017' ] ?? "0", "v{0;0;200}" );

$oFC->page_content [ 'c020' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'c020' ] ?? "0", "v{0;0;100}" );
$oFC->page_content [ 'c021' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'c021' ] ?? "0", "v{0;0;100}" );

$localHulpCheck1 = str_replace ( ".html", "", substr ($oFC->page_content [ 'z100' ], 5 ) );

if ( $oFC->page_content [ 'z101' ] != $localHulpCheck1 &&  $_POST [ 'command' ] != "Save" ) { // andere selectie behalve andere parameter om te saven
	$oFC->page_content [ 'z101' ] = $localHulpCheck1;
	$from = $oFC->setting [ 'frontend' ];
	$LocalHulp = sprintf ("%s%sz%s.html",$oFC->setting [ 'frontend' ], $default_prefix, $localHulpCheck1 );
	if ( file_exists ( $LocalHulp ) ) {
		if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array( $oFC->page_content, $LocalHulp, $_POST ), __LINE__ . __FUNCTION__ ); 
		$LocalHulp1 = file_get_contents ( $LocalHulp );	
		$oFC->page_content = array_merge ( $oFC->page_content, json_decode ( $LocalHulp1 , true ) );
	} else {
		$oFC->page_content [ 'z101' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'z101' ] ?? "--", "s{ TOASC|CLEAN|TRIM }" );
		$oFC->page_content [ 'z102' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'z102' ] ?? "0", "v{0;0;100}" );
	}
} else {  // parameeter wordt overschreven of blijft dezelfde
	$oFC->page_content [ 'z101' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'z101' ] ?? "--", "s{ TOASC|CLEAN|TRIM }" );
	$oFC->page_content [ 'z102' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'z102' ] ?? "0", "v{0;0;100}" );
}
?>