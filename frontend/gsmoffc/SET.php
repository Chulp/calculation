<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$FC_SET [ 'version' ] 		= "frontend v20240210";
$FC_SET [ 'SET_function' ] 	= 'setupc/dummy'; 	// backend menu ';
if ( $section_id == 17 ) { 
	$FC_SET [ 'SET_menu' ] 		= 'calculation';  // frontend menu ';
} else { 
	$FC_SET [ 'SET_menu' ] 		= 'antropometrie';  // frontend menu ';
}

// for the administrator and the editor
if ( isset ($_SESSION [ 'GROUPS_ID' ] ) && ( $_SESSION [ 'GROUPS_ID' ] == 1 || 	$_SESSION [ 'GROUPS_ID' ] == 4 ) )  {
	$FC_SET [ 'SET_menu' ] 		= 'antropometrie|transitie|calculation'; 
	$FC_SET [ 'SET_function' ] 	= 'setupc|antropometrie|transitie|calculation';
}

// on the screen there may appear a module name to select.
$FC_SET [ 'SET_txt_menu' ] [ 'xsetupc' ]	= 'Instellingen';
$FC_SET [ 'SET_txt_menu' ] [ 'xdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vtransitie' ]	= 'Transitie calculatie';
$FC_SET [ 'SET_txt_menu' ] [ 'vantropometrie' ]	= 'BMI berekening';
$FC_SET [ 'SET_txt_menu' ] [ 'vcalculation' ]	= 'calculation';
$FC_SET [ 'SET_txt_menu' ] [ 'xtransitie' ]	= 'Transitie calculatie';
$FC_SET [ 'SET_txt_menu' ] [ 'xantropometrie' ]	= 'BMI berekening';
$FC_SET [ 'SET_txt_menu' ] [ 'xcalculation' ]	= 'voorbeeld';

?>
