<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content,   $oFC->language ['trans'] ), __LINE__ . __FUNCTION__ ); /* debug */ 

// pensioendatum
$oFC->page_content [ 'c023' ] = $oFC->Gsm_pensioenDatum  ( $oFC->page_content [ 'b002' ] );

// uitvoer
// algemeen
// $proces_version = "today";
$oFC->page_content [ 'RAPPORTAGE' ] .= '<table>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6"><h3>Berekening Transitievergoeding (ex artikel 7:673 BW)</h3></td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">Datum berekening : ' . $oFC->gsm_sanitizeStrings ( date ( "d M Y", time ( ) ), "s{ DATUM }" ) . '</td><td colspan="4"></td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';

$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Gegevens Werknemer</h4></td><td colspan="4"></td></tr>';	
if ( strlen ( $oFC->page_content [ 'b001' ] ) > 3 ) $oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">Naam : </td><td></td><td colspan="2"><strong>' . $oFC->page_content [ 'b001' ] . '</strong></td><td></td></tr>';	
if ( $oFC->page_content [ 'b002' ] != $oFC->language [ 'trans' ][ 'b002' ] )	
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Geboorte datum : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'b002' ], "s{ DATUM }" ) . '</td><td></td></tr>';	
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">Datum in dienst : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'b020' ], "s{ DATUM }" ) . '</td><td></td></tr>';	
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">Datum uit dienst : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'b021' ], "s{ DATUM }" ) . '</td><td></td></tr>';	
if ( $oFC->page_content [ 'b002' ] != $oFC->language [ 'trans' ][ 'b002' ] )	{
	if ( date ( "Y-m-d", strtotime( '-1 year', strtotime ( $oFC->page_content [ 'c023' ] ) ) ) < $oFC->page_content [ 'b021' ]) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Pensioendatum : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'c023' ], "s{ DATUM }" ) . '</td><td></td></tr>';	
	}
}
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">Bruto Maandsalaris : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'b030' ], "s{ KOMMA|EURO }" )  . '</td><td></td></tr>';	

$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';

if ( strlen ( $oFC->page_content [ 'b010' ] ) >3 ) {
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Naam Werkgever</h4></td><td></td><td colspan="2"><strong>' . $oFC->page_content [ 'b010' ]. '</strong></td></tr>';	
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';
}


$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Duur Dienstverband</h4></td><td></td><td colspan="2"></td><td></td></tr>';	

// berekening dienstverband
$Lhulp01 =substr ( $oFC->page_content [ 'b020' ],0,4); // jaartal in dienst
$Lhulp02 =substr ( $oFC->page_content [ 'b020' ],5,2); // maand
$Lhulp03 =substr ( $oFC->page_content [ 'b020' ],8,2); //dag
$Lhulp10 = date ( "Y-m-d", strtotime( '+1 day', strtotime ( $oFC->page_content [ 'b021' ] ) ) ); // ingave is laaste dag in dienst de volgende dag is echt uit diienst 
$Lhulp11 =substr ( $Lhulp10,0,4); // jaatal echt uit dienst 
$Lhulp12 =substr ( $Lhulp10,5,2); // maand
$Lhulp13 =substr ( $Lhulp10,8,2); //dag
$oFC->page_content [ 'c020' ] = $Lhulp11 - $Lhulp01; // aantal jaren
$oFC->page_content [ 'c021' ] = $Lhulp12 - $Lhulp02; // aantal maanden
$oFC->page_content [ 'c022' ] = $Lhulp13 - $Lhulp03; // aantal dagen
if ($oFC->page_content [ 'c022' ] < 0) {  // correctie negatief aantal dagen
	$oFC->page_content [ 'c021' ] = $oFC->page_content [ 'c021' ] -1; //maand eraf
	$oFC->page_content [ 'c022' ] = $oFC->page_content [ 'c022' ] +30; //30 dagen  erbij
	// er zit geen correctie in voor aantal dagen in de maand als de maand vorafgaand aan de datum uit dienst 28 29 31 dagen heeft kan het er max 3 dagen langs zitten 
}
if ($oFC->page_content [ 'c021' ] < 0) {  // correctie negatief aantal maanden
	$oFC->page_content [ 'c020' ] = $oFC->page_content [ 'c020' ] -1; //jaar eraf
	$oFC->page_content [ 'c021' ] = $oFC->page_content [ 'c021' ] +12; //12 maanden erbij
}
// er een string van maken 
$Lhulp00 = sprintf ( "%s jaren, %s maanden en %s dagen" , $oFC->page_content [ 'c020' ], $oFC->page_content [ 'c021' ], $oFC->page_content [ 'c022' ] );

$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">Duur dienstverband : </td><td></td><td colspan="3">' . $Lhulp00 . '</td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';

$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Berekening Transitevergoeding</h4></td><td colspan="4"></td></tr>';	

// berekening jaren
$Lhulp02 = $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'b030' ], "s{ KOMMA|EURO }" );
$oFC->page_content [ 'c025' ] = 0;
if ($oFC->page_content [ 'c020' ] >0 ) {
	$Lhulp00 = sprintf ( "%s jaren x %s x 1/3" , $oFC->page_content [ 'c020' ], $Lhulp02 );
	$Lhulp01 = round ( $oFC->page_content [ 'b030' ] * $oFC->page_content [ 'c020' ] / 3, 2 );
	$oFC->page_content [ 'c025' ] = $oFC->page_content [ 'c025' ] + $Lhulp01;
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">' .$Lhulp00 .' : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $Lhulp01, "s{ KOMMA|EURO }" ) . '</td><td></td></tr>';
}
if ($oFC->page_content [ 'c021' ] >0 ) {
	$Lhulp00 = sprintf ( "%s maanden x %s x 1/3 x 1/12" , $oFC->page_content [ 'c021' ], $Lhulp02 );
	$Lhulp01 = round ( $oFC->page_content [ 'b030' ] * $oFC->page_content [ 'c021' ] / 36 , 2 );
	$oFC->page_content [ 'c025' ] = $oFC->page_content [ 'c025' ] + $Lhulp01;
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">' .$Lhulp00 .' : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $Lhulp01, "s{ KOMMA|EURO }" ) . '</td><td></td></tr>';
}
if ($oFC->page_content [ 'c022' ] >0 ) {
	$Lhulp00 = sprintf ( "%s dagen x %s x 1/3 x 1/365" , $oFC->page_content [ 'c022' ], $Lhulp02 );
	$Lhulp01 = round ( $oFC->page_content [ 'b030' ] * $oFC->page_content [ 'c022' ] / 3 / 365 , 2 );
	$oFC->page_content [ 'c025' ] = $oFC->page_content [ 'c025' ] + $Lhulp01;
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">' . $Lhulp00 . ' : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $Lhulp01, "s{ KOMMA|EURO }" ) . '</td><td></td></tr>';
}

$Lhulp1x =  $oFC->gsm_sanitizeStringv ( $oFC->page_content [ 'c001' ] ?? "0", "v{". $oFC->page_content[ 'c001' ].";0;100000}" );
if ( $Lhulp1x < $oFC->page_content [ 'c025' ] ) {
	$oFC->page_content [ 'c025' ] = $Lhulp1x;
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">De transitievergoeding is in '. $oFC->page_content [ 'c003' ] . ' gemaximeerd op ' . $oFC->gsm_sanitizeStrings ( $Lhulp1x, "s{ WHOLE|EURO }" ) . '<br/> Voor andere jaren is wellicht een ander maximum van toepassing.</td></tr>';
}
	if ( $oFC->page_content [ 'b002' ] != $oFC->language [ 'trans' ][ 'b002' ] ) 	{
	if ( date ( "Y-m-d", strtotime ( $oFC->page_content [ 'c023' ] ) ) < $oFC->page_content [ 'b021' ]) {
		$oFC->page_content [ 'c025' ] = 0;
		$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">Na pensioendatum is er geen transitievergoeding regeling</td></tr>';
	}
}	
if ( $oFC->page_content [ 'b021' ] < "2020-01-01" )	{
	$oFC->page_content [ 'c025' ] = 0;
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">Voor deze datum uit dienst is er een afwijkende regeling</td></tr>';
}

$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';

$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Transitievergoeding</h4></td><td></td><td style="text-align:right;" colspan="2"><h4>' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'c025' ], "s{ KOMMA|EURO }" ) . '</h4></td><td></td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '</table>';

$oFC->page_content [ 'c001' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'c001' ], "s{ KOMMA|EURO }" );
$oFC->page_content [ 'b030' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'b030' ], "s{ KOMMA|EURO }" );