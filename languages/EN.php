<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$MOD_GSMOFFC = array(
	'OWN' => "MOD_GSMOFFC",
	'LANG' => "EN",
	'VERS' => "v20240210",
		
	'active' => array ( 
		'0' => 'niet actief', 
		'1' => 'actief'),

	'beweging' => array ( 
		'a0' => 'Geen of weinig beweging', 
		'a1' => '1-3 keer per week oefeningen', 
		'a2' => '4-5 keer per week oefeningen', 
		'a3' => 'Dagelijks oefeningen of 3-4 keer per week sport', 
		'a4' => '6-7 keer per week sport', 
		'a5' => 'Dagelijks flinke training of zwaar lichamelijk werk'),
		
	'beweging2' => array ( 
		'a0' => '1.2', 
		'a1' => '1.375', 
		'a2' => '1.465', 
		'a3' => '1.55', 
		'a4' => '1,725', 
		'a5' => '1.9'),		
			
	'BMI'=> array (
			'16' => "Ondergewicht..",   
			'17' => "Ondergewicht. ",  
			'18.5' => "Ondergewicht", 
			'25' => "Normaal",	
			'27' =>	"Overgewicht",	
			'30' => "Licht Overgewicht", 
			'35' => "Matig Overgewicht", 
			'40' => "Ernstig Overgewicht", 			
			'100' => "Ziekelijk Overgewicht"), 
		
	'BMI70'=> array (  // aangepast voor boven de 70jaar
			'16' => "Ondergewicht..",   
			'17' => "Ondergewicht. ",  
			'22' => "Ondergewicht", 
			'28' => "Normaal",	
			'30' =>	"Overgewicht",	
			'40' => "Ernstig Overgewicht", 			
			'100' => "Ziekelijk Overgewicht"), 

		
	'MHR' => "Buikholtevet % te hoog",
			
	"buikman" => array (
			'94' => "Laag risico",   //te lage buikomvang
			'100' => "Verhoogd risico",    // normale buikomvang
			'200' => "Sterk verhoogd risico "), 	// te hoge buikomvang	
			
	"20man" => array (
			'8' => "Te laag",   
			'19.9' => "Normaal",   
			'25' => "Te hoog", 
			'100' => "Zeer hoog"),
			
	"20vrouw" => array (
			'21' => "Te laag",   
			'33.9' => "Normaal",   
			'39' => "Te hoog", 
			'100' => "Zeer hoog"),
			
	"40man" => array (
			'11' => "Te laag",   
			'22.9' => "Normaal",   
			'28' => "Te hoog", 
			'100' => "Zeer hoog"),
			
	"40vrouw" => array (
			'23' => "Te laag",   
			'34.9' => "Normaal",   
			'40' => "Te hoog", 
			'100' => "Zeer hoog"),
			
	"60man" => array (
			'13' => "Te laag",   
			'24.9' => "Normaal",   
			'30' => "Te hoog", 
			'100' => "Zeer hoog"),
			
	"60vrouw" => array (
			'24' => "Te laag",   
			'36.9' => "Normaal",   
			'42' => "Te hoog", 
			'100' => "Zeer hoog"),
			
	"buikvrouw" => array (
			'80' => "Laag risico",   //te lage buikomvang
			'90' => "Verhoogd risico",    // normale buikomvang
			'200' => "Sterk verhoogd risico "), 	// te hoge buikomvang	

	"knijpman" => array (  // 10 percentiel waarde
			'5' => 6,   
			'10' => 12,   
			'15' => 21, 
			'20' => 30,
			'25' => 36,
			'30' => 38,
			'35' => 39,
			'40' => 38,
			'45' => 36,
			'50' => 35,
			'55' => 34,
			'60' => 33,
			'65' => 31,
			'70' => 29,
			'75' => 26,
			'80' => 23,		
			'85' => 19,
			'90' => 16,
			'95' => 15 ),
			
	"knijpvrouw" => array (  // 10 percentiel waarde
			'5' => 6,   
			'10' => 12,   
			'15' => 17, 
			'20' => 21,
			'25' => 23,
			'30' => 24,
			'35' => 23,
			'40' => 23,
			'45' => 22,
			'50' => 21,
			'55' => 19,
			'60' => 18,
			'65' => 17,
			'70' => 16,
			'75' => 14,
			'80' => 13,		
			'85' => 11,
			'90' => 9,
			'95' => 8 ),	
			
	'antropo' => array (

		'z001' => "1950-01-01",		
		'z002' => "1925-01-01", 
		'z003' => "1940-01-01", 		
		't001' => "Voor documentatie en identificatie doeleinden", 
		't002' => "Geboortedatum voor berekening en identificatie, formaat jjjj-mm-dd", 
		't009' => "Geslacht voor berekening", 
		'b020' => "Gewicht voor berekening (in kg, decimalen toegestaan)", 
		'b021' => "Lengte voor berekening (in cm, decimalen toegestaan)", 
		'b022' => "Buikomvang voor berekening (in cm, decimalen toegestaan, te meten halverwege tussen onderkant ribben bovenkant heupbeen)",
		'b023' => "Nek wijdte voor berekening (in cm, decimalen toegestaan)",
		'b024' => "Heup wijdte voor berekening (in cm, decimalen toegestaan)",
		'b026' => "Handknijpkracht (in kg, decimalen toegestaan, hoogste waarde van de metingen)",
		'b025' => "Activiteit > Oefeningen = 15-30 min verhoogd hartritme, Sport = >40 minuten verhoogd hartritme, Training = >120 minuten verhoogd hartritme" ,
		'c002' => 0,   	
		'z020' => "75",
		'z021' => "175",
		'z022' => "100",
		'z023' => "40",
		'z024' => "100"	),	// jaar
		
	'trans' => array (
		't001' => "Voor documentatie en identificatie doeleinden", 
		't002' => "Geboortedatum voor berekening en identificatie, formaat jjjj-mm-dd", 
		't020' => "Eerste dag in dienst, formaat jjjj-mm-dd", 
		't021' => "Laatste dag in dienst, formaat jjjj-mm-dd", 
		't030' => "Bruto maandsalaris + 1/12 deel van vakantiegeld, bonus, 13e maand en overwerk",	
		'c001' => "94000",   // max jaarsalaria	
		'b002' => "1950-01-01",  		
		'c002' => 0,  
		'c003' => "2024"	),	// jaar
		
	'DUMMY' => array (
		'0' => 'Geen functionaliteit. Database niet geinitialiseerd, geen toegang of andere settings niet juist',
		'1' => 'Dummy module zonder functionaliteit is nu gestart',
		'2' => 'Verifieer of de settingsfunctionaliteit van deze module juist gedaan is'
		),

	'functies' => array (
		'0' => 'blue', 
		'1' => 'olive', 
		'2' => 'olive', 
		'3' => 'red' , 
		'4' => 'orange', 
		'5' => 'grey' , 
		'6' => 'grey',
		'7' => 'black'),

		
	'layout' => array (
		'aanv0' => '<p>aanvulling %s voor %s ( %s ) door %s </p><p>%s</p><hr />',
		'show0' => '<div class="ui text container"><div class="ui segment">',
		'show9' => '</div></div>',
		'show1' => '' ),  
		
	'aanvulling' =>	'<p>aanvulling %s voor %s ( %s ) door %s </p><p>%s</p><hr />',		
		
	'line_color' => array( 
		0 => '', 
		1 => 'bgcolor="#eeeeee"', 
		2 => 'bgcolor="#dddddd"', 
		3 => 'bgcolor="#cccccc"', 
		4 => 'bgcolor="#bbbbbb"'),
		
	'pdf'	=> array ( 
		'0' => "Document created on : ", 
		'1' => "Aantal regels verwerkt: ",
		'2' => "Selected options: ",
		'3' => "Modules versions: " ),
		
	'PDF_TAIL' => array(
		'TOTAL' => 		"Total records : ",
		'MAILED' => 	"Records mailed : ",	
		'POSTED' => 	"Records to be posted : ",
		'UNSELECTED' => "Records unselected : ",
		'SELECTED' => 	"Shares selected : ",
		'NOT_SELECTED' => "Shares not selected : ",
		'MAILING' => 	"Mailing on : ",
		'SELECTION' => 	"Selection : ",
		'EVERYBODY' => 	" Iedereen ",
		'EVERY_MAIL' => " Iedereen met mailadres ",
		'ALL_MEMBERS' =>" Alle leden ",
		'REMINDER' => 	" Reminder ",
		'REFERENCED' => " Referenced ",
		'SHAREHOLDERS' => " Aandeelhouders ",
		'STANDEN' => " Afhankelijk van standen "),

	'tbl_icon' => array ( 
		1 =>'View', 
		2 =>'Reset', 
		3 =>'Add',
		4 =>'Save',  
		5 =>'Save (as new)', 
//		6 =>'Remove', 
//		7 =>'Calculate',
//		8 =>'Check',
		9 =>'Select', 
//		10 =>'+',
		11 =>'Print', 
		12 =>'Set',
//		13 =>'reserved',
//		14 =>'Next',
//		15 =>'Test',
//		16 =>'Mail',
		17 =>'Process', 
		18 =>'Invoicing', 
//		19 =>'Balans', 
//		20 =>'Result' ,
		21 =>'Verwerk'
	),  

	'DATABASE UPDATE' 	=> ' Database records adapted : ',

	'TXT_ACTIVE_DATA'	=> ' Actief record gevonden' ,	
	'TXT_CONSISTENCY'	=> ' Oeps consistency controle',
	'TXT_DATABASE_NEW'	=> ' Initial record added ',
	'TXT_DIR_CREATION' 	=> ' Directory aangemaakt',
	'TXT_ERROR_ADRES'	=> ' Oeps name and / or address data missing',
	'TXT_ERROR_DATA' 	=> ' Oeps geen data ',  
	'TXT_ERROR_DATABASE' => ' Oeps inconsisten database field  ',
	'TXT_ERROR_INIT'	=> ' Oeps systeem niet geinitialiseerd en/of lege database ',
	'TXT_ERROR_SIPS'	=> ' Oeps sips active ',
	'TXT_ERROR_PAGE'	=> ' Oeps unexpected situation ',		
	'TXT_LOGIN' 		=> ' Login',
	'TXT_LOGIN_ERROR' 	=> ' Not a valid e-mail address or existing already or password too short.',
	'TXT_LOGIN_NOW' 	=> ' Uw login data is aangepast. Login met uw nieuwe gegevens. ',
	'TXT_LOGIN_REGISTER' => ' Register / Change Password',
	'TXT_LOGIN_SETT'	=> ' Correct Login Settings  ',
	'TXT_LOGIN_VERIFY' 	=> ' Verificatie ',
	'TXT_MAINTENANCE' 	=> ' Maintenance ', 
	'TXT_NO_ACCESS'		=> '(Partner) Access not available ',
	'TXT_REC_CHANGE'	=> ' Aantal records aangepast : ',	
	'TXT_REMOVE_INPUT'	=> 'Invoerdata verwijderd',
	'TXT_REMOVE_REF'	=> 'weg',
	'TXT_REMOVE_KEYWORD'=> 'recycle',
	'TXT_SETUP' 		=> ' Setup ',

	'cal' => array ( 
		'c001' => "P",
		'c002' => "Scenario naam", 
		'c003' => "voornaam achternaam", 
		'c009' => "man", 
		'c006' => "1930-01-01",  	// default geb datum	
		'c025' => "a1"
		),	

);

?>