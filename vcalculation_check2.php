<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content, $oFC->language, $_POST ), __LINE__ . __FUNCTION__ ); 

/* settings */
$oFC->page_content [ 'c099' ] = true ;  // leeftijd buiten range 
$oFC->page_content [ 'c098' ] = true ;  // toon alle defects
$oFC->page_content [ 'c097' ] = false ; // Buikomvang niet beschikbaar
if ( $oFC->page_content [ 'c015' ] > 10 ) $oFC->page_content [ 'c097' ] = true ; // Buikomvang beschikbaar
$oFC->page_content [ 'c096' ] = false ;  // Vormfactor beschikbaar

$TEMPLATE2 = '<tr><td colspan="2">%s</td><td style="text-align:right;" colspan="2">%s<td colspan="2">%s</td></tr>';
$TEMPLATE6 = '<tr><td colspan="6">%s</td></tr>';
$TEMPLATE0 = '<tr><td colspan="6"><hr /></td></tr>';


$oFC->page_content [ 'RAPPORTAGE' ] .= '<table>';
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h3>Antropometrie Meting / Overzicht</h3>");

/* identificatie */
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Datum analyse : ", 
										$oFC->gsm_sanitizeStrings ( date ( "d M Y", time ( ) ), "s{ DATUM }" ),
										"( ref:".$oFC->page_content [ 'c001' ] . " )");
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");

/* persoonlijke gegevens */
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Gegevens persoon</h4>" );
if ( strlen ( $oFC->page_content [ 'c003' ] ) > 3 ) {
	if ( $oFC->page_content [ 'c003' ] != $oFC->language [ 'cal' ][ 'c003' ] )	
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Naam : ", 
										$oFC->page_content [ 'c003' ],
										"" );
}										
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Geboorte datum : ",
										$oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'c006' ], "s{ DATUM }" ),
										"" );	
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Geslacht : ",
										$oFC->page_content [ 'c009' ],
										"" );										
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");

/* metingen */
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Metingen</h4>" );
if ( $oFC->page_content [ 'c010' ] > 4 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Gewicht : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c010' ] , "s{ KOM1 }" ) ,
										" kg." );

if ( $oFC->page_content [ 'c011' ] > 40 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Lengte : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c011' ] , "s{ KOM1 }" ),
										" cm." );
										
if ( $oFC->page_content [ 'c015' ] > 10 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Buikomvang : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c015' ] , "s{ KOM1 }" ),
										" cm." );
									
if ( $oFC->page_content [ 'c017' ] > 10 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Heup wijdte : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c017' ] , "s{ KOM1 }" ),
										" cm." );
										
if ( $oFC->page_content [ 'c016' ] > 10 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Nek Wijdte : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c016' ] , "s{ KOM1 }" ),
										" cm." );
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");

/* Handknijpkracht */	
if ( $oFC->page_content [ 'c020' ] > 2 || $oFC->page_content [ 'c021' ] > 2 ) {

	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Handknijpkracht</h4>" );
	if ( $oFC->page_content [ 'c020' ] > 2 )
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Links : ", 
											$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c020' ] , "s{ KOM1 }" ),
											" kg." ); 
										
	if ( $oFC->page_content [ 'c021' ] > 2 )
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Rechts : ", 
											$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c021' ] , "s{ KOM1 }" ),
											" kg." ); 
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
}

$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Opgave</h4>" );
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Activiteit level : ", 
									$oFC->language [ 'beweging' ] [ $oFC->page_content [ 'c025' ] ],
									"" ); 
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
	
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Berekeningen</h4>" );

/* 	c030 berekening leeftijd  
 *	c099 binnen range) 
 */
$Lhulp01 = date ( "Y-m-d", strtotime ( $oFC->page_content [ 'c006' ] ) ) ; 	// begin
$Lhulp02 = date ( "Y-m-d", time ( ) ); 		// eind
$LhulpLow = 20 ; //ondergrens
$LhulpHigh = 100 ; // bovengrens

$Lhulp01b =substr ( $Lhulp01, 0, 4); // jaartal begin
$Lhulp01c =substr ( $Lhulp01, 5, 2); // maandbegin
$Lhulp01d =substr ( $Lhulp01, 8, 2); //dag begin
/* volgende dag nodig */
$Lhulp02 = date ( "Y-m-d", strtotime( '+1 day', strtotime ( $Lhulp02 ) ) ); // als ingave is laatste dag dan is de volgende dag het meetpuntlaaste dag in dienst de volgende dag is echt uit diienst 
/* end dagcorrectie */
$Lhulp02b = substr ( $Lhulp02, 0, 4 ); // jaartal begin
$Lhulp02c = substr ( $Lhulp02, 5, 2 ); // maandbegin
$Lhulp02d = substr ( $Lhulp02, 8, 2 ); //dag begin

/* aantal dagen maanden en jaren berekening */
$Lhulp03b = $Lhulp02b - $Lhulp01b; // aantal jaren
$Lhulp03c = $Lhulp02c - $Lhulp01c; // aantal maanden
$Lhulp03d = $Lhulp02d - $Lhulp01d; // aantal dagen
if ( $Lhulp03d < 0 ) {  // correctie negatief aantal dagen
	$Lhulp03c = $Lhulp03c - 1 ; //  maand eraf
	$Lhulp03d = $Lhulp03d + 30 ; // 30 dagen  erbij
	// er zit geen correctie in voor aantal dagen in de maand als de maand voorafgaand 28 29 31 dagen kan het er max 3 dagen langs zitten 
}
if ( $Lhulp03c < 0) {  // correctie negatief aantal maanden
	$Lhulp03b = $Lhulp03b -1; //jaar eraf
	$Lhulp03c = $Lhulp03c + 12 ; //12 maanden erbij
}
/* van de leeftijd of de duur een string van maken */
$Lhulp00 = sprintf ( "%s jaren, %s maanden en %s dagen" , $Lhulp03b, $Lhulp03c, $Lhulp03d );
//$Lhulp00 = sprintf ( "%s jaar " , $Lhulp03b, $Lhulp03c, $Lhulp03d );
$oFC->page_content [ 'c030' ] = $Lhulp03b;
$oFC->page_content [ 'c099' ] = true ;  // leeftijd buiten range
if ( $Lhulp03b > $LhulpLow && $Lhulp03b < $LhulpHigh ) $oFC->page_content [ 'c099' ] = false ;

if ( $oFC->page_content [ 'c099' ] ) {
	/* niet ingegeven  of buiten range */
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Leeftijd : ", 
										$Lhulp00,
										"<strong>Buiten reken / analyse grenzen</strong>" ); 
} else {
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Leeftijd : ", 
										$oFC->page_content [ 'c030' ],
										" jaar" ); 	
} 

/* 	c031 berekening BMI  */ 

$Lhulp01 =  $oFC->page_content [ 'c011' ] / 100 ;
$oFC->page_content [ 'c031' ] = round ( $oFC->page_content [ 'c010' ] / $Lhulp01  / $Lhulp01, 1 );

/* c032 qualificatie bij BMI 
 * there are different tables depening on age 
 * c033 ondergrens BMI 
 * c034 bovengrens BMI */
$tableBMI = array (
	'16' => "Ondergewicht..",   
	'17' => "Ondergewicht. ",  
	'18.5' => "Ondergewicht", 
	'25' => "Normaal",	
	'27' =>	"Overgewicht",	
	'30' => "Licht Overgewicht", 
	'35' => "Matig Overgewicht", 
	'40' => "Ernstig Overgewicht", 			
	'100' => "Ziekelijk Overgewicht");
$tableBMI70 = array (  // aangepast voor boven de 70+
	'16' => "Ondergewicht..",   
	'17' => "Ondergewicht. ",  
	'22' => "Ondergewicht", 
	'28' => "Normaal",	
	'30' =>	"Overgewicht",	
	'40' => "Ernstig Overgewicht", 			
	'100' => "Ziekelijk Overgewicht");
if ( $oFC->page_content [ 'c030' ] < 70 ) {
	$temp =  $tableBMI;
	$oFC->page_content [ 'c033' ] = 18.5; 
	$oFC->page_content [ 'c034' ] = 25;
} else {
	$temp =  $tableBMI70;
	$oFC->page_content [ 'c033' ] = 22; 
	$oFC->page_content [ 'c034' ] = 28;
}

$oFC->page_content [ 'c032' ] = "";
$switch = true;
foreach ( $temp as $key => $value ) {
	if ( $switch ) {
		if ( $key >= $oFC->page_content [ 'c031' ] ) $switch = false;
		$oFC->page_content [ 'c032' ] = $value;
	}
}
/* c033 gewicht bij BMI ondergrensgrens 
 * c034 gewicht bij BMI bovengrens */
$oFC->page_content [ 'c033' ] = round ( $oFC->page_content [ 'c033' ] * $Lhulp01  * $Lhulp01, 0 );
$oFC->page_content [ 'c034' ] = round ( $oFC->page_content [ 'c034' ] * $Lhulp01  * $Lhulp01, 0 );


/* c035 wat moet eraf erbij */
if  ( $oFC->page_content [ 'c010' ] > $oFC->page_content [ 'c034' ] ) { // eraf
	$oFC->page_content [ 'c035' ] = floor ( $oFC->page_content [ 'c034' ] - $oFC->page_content [ 'c010' ] );
} elseif  ( $oFC->page_content [ 'c010' ] < $oFC->page_content [ 'c033' ] ) { // erbij
	$oFC->page_content [ 'c035' ] = floor ($oFC->page_content [ 'c010' ] - $oFC->page_content [ 'c033' ]);
} else {
	$oFC->page_content [ 'c035' ] = 0;
}

/* suppress fault message if BMI OK 
 * c098 toon alle defects
 */
if ( strpos ( "normaal", strtolower ($oFC->page_content [ 'c032' ] ) ) !== false) $oFC->page_content [ 'c098' ] = false;

/* display */

if ( $oFC->page_content [ 'c099' ] ) {
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Gewichts analyse</h4>" );
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Body Mass Index (BMI) : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c031' ] , "s{ KOM1 }" ),
										$oFC->page_content [ 'c032' ] ); 
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Bij uw lengte past een gewicht van : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c033' ] , "s{ WHOLE }" ) . 
										" - " . 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c034' ] , "s{ WHOLE }" ),
										" kg." ); 	
	if ( $oFC->page_content [ 'c035' ] > 1 ) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Er zou dus bij mogen : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c035' ] , "s{ WHOLE|STRONG }" ),
										" kg." );
	}
	if ( $oFC->page_content [ 'c035' ] < - 1 ) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Er zou dus af mogen : ", 
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c035' ] * -1 , "s{ WHOLE|STRONG }" ),
										" kg." );
	}
}

									
if ( $oFC->setting [ 'debug' ] == "yes" ) {
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
	$LocalHulpT = sprintf ( "berekening: BMI = %s / %s / %s",	$oFC->page_content [ 'c010' ], $Lhulp01, $Lhulp01);							
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, $LocalHulpT,
										$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c031' ] , "s{ KOM1 }" ),
										"" );
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "BMI tabel gebruikt" );
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Leeftijd : ", 
									$oFC->page_content [ 'c030' ],
									" jaar" ); 	
	foreach ($temp as $key => $value ) { 
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "tot BMI : ".$key, $value , ""  );
}	}


if ( $oFC->page_content [ 'c097' ] ) {
	/* c038 qualificatie bij buikomvang 
	 * c039 advies buikomvang
	 * there are different tables depening on gender */	
	$Tablebuikman = array (
				'94' => "Laag risico",   			// te lage buikomvang
				'100' => "Verhoogd risico",    		// normale buikomvang	
				'120' => "Verhoogd risico ..",    	// te hoge buikomvang					
				'200' => "Sterk verhoogd risico ", 	// te hoge buikomvang	
				);
	$Tablebuikvrouw = array (
				'80' => "Laag risico",   			// te lage buikomvang
				'90' => "Verhoogd risico",    		// normale buikomvang
				'110' => "Verhoogd risico ..",     		// te hoge buikomvang	
				'200' => "Sterk verhoogd risico " 	// te hoge buikomvang	
	);
	if  ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) { //  "man"
		$temp =$Tablebuikman;
	} else {
		$temp =$Tablebuikvrouw;
	}

	$oFC->page_content [ 'a038' ] = "";
	$switch = true;
		foreach ( $temp as $key => $value ) {
			if ( $switch ) {
				if ( $key >= $oFC->page_content [ 'c015' ] ) $switch=false;
				if ( $switch ) $oFC->page_content [ 'c038' ] = $value;
	}	}	 
		
	$oFC->page_content [ 'c039' ] = "";
	if  ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) { //  "man"
		$oFC->page_content [ 'c039' ] = "94 - 99";
	} else { 
		$oFC->page_content [ 'c039' ] = "80 - 89";
	}
		
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Buikomvang analyse</h4>" );

	if ( $oFC->page_content [ 'c015' ] > 20 ) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Buikomvang ", 
											$oFC->page_content [ 'c015' ],
											($oFC->page_content [ 'c098' ] ) ? $oFC->page_content [ 'c038' ] : "" ); 			
	}
		
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Bij u past een buikomvang van ", 
											$oFC->page_content [ 'c039' ],
											" cm." ); 

	if ( $oFC->setting [ 'debug' ] == "yes" ) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "buikomvang tabel gebruikt" );
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Gender : ", 
											$oFC->page_content [ 'c009' ],
											"" ); 
									
		foreach ($temp as $key => $value ) { 
			$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "buikomvang vanaf : ".$key . " cm.", $value , ""  );
}	}	}
	
/* Lichaamsvet * /
/* c040 vet percentage BMI methode */
$Lhulp01 = $oFC->page_content [ 'c031' ]; // BMI
$Lhulp02 = $oFC->page_content [ 'c030' ]; // leeftijd
if  ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) { //  "man"
	$Lhulp03 = $oFC->page_content [ 'c031' ] * 1.2; 
	$Lhulp04 = $oFC->page_content [ 'c030' ] * 0.23; 
	$oFC->page_content [ 'c040' ] = round ($Lhulp03 + $Lhulp04 - 16.2, 1);
	$LocalHulpT1 = sprintf ( "berekening 1 : vet pct = ( %s * %s ) + ( %s * %s ) - %s",	
					$oFC->page_content [ 'c031' ], 
					"1,2",
					$oFC->page_content [ 'c030' ],
					"0,23",
					"16,2");	
} else {
	$Lhulp03 = $oFC->page_content [ 'c031' ] * 1.2; 
	$Lhulp04 = $oFC->page_content [ 'c030' ] * 0.23; 
	$oFC->page_content [ 'c040' ] = round ($Lhulp03 + $Lhulp04 - 5.4, 1);
	$LocalHulpT1 = sprintf ( "berekening 1 : vet pct = ( %s * %s ) + ( %s * %s ) - %s",	
					$oFC->page_content [ 'c031' ], 
					"1,2",
					$oFC->page_content [ 'c030' ],
					"0,23",
					"5,4");
}	
	
/* c041 brekening vet percentage op metingen methode	
 * c044 BHR buikholte vet */
$oFC->page_content [ 'c096' ] = false;
$Lhulp01 = $oFC->page_content [ 'c011' ] ; // lengte
$Lhulp02 = $oFC->page_content [ 'c015' ] ; // buik
$Lhulp03 = $oFC->page_content [ 'c016' ] ; // boord
$Lhulp04 = $oFC->page_content [ 'c017' ] ; // heup

if  ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) { //  "man"
	$Lhulp05 = $Lhulp01 * $Lhulp02 * $Lhulp03;
	if ( $Lhulp05 > 0 ) $oFC->page_content [ 'c096' ] = true;
} else {
	$Lhulp05 = $Lhulp01 * $Lhulp02 * $Lhulp03 * $Lhulp04;
	if ( $Lhulp05 > 0 ) $oFC->page_content [ 'c096' ] = true;
}

$oFC->page_content [ 'c041' ] = 1;
$oFC->page_content [ 'c043' ] = 1;
$oFC->page_content [ 'c044' ] = 0;
	
if ( $oFC->page_content [ 'c096' ] ) {
	if  ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) { //  "man"
		$Lhulp06 = $Lhulp02 - $Lhulp03;
		$Lhulp06 = log ($Lhulp06, 10);
		$Lhulp06 = 0.19077 * $Lhulp06;
		$Lhulp07 = log ($Lhulp01, 10);
		$Lhulp07 = 0.15456 * $Lhulp07;
		$Lhulp08 = 1.0324 - $Lhulp06  + $Lhulp07;
		$oFC->page_content [ 'c041' ] = round (495 / $Lhulp08 - 450, 1);
		$LocalHulpT2 = sprintf ( "berekening 2 : vet pct =  %s / ( %s - %s * log ( %s - %s ) + %s * log ( %s ) ) - %s",
			"495",
			"1.0324",
			"0.19077",
			$Lhulp02,
			$Lhulp03,
			"0.15456",
			$Lhulp01,
			"450");
		if ($oFC->page_content [ 'c017' ] > 20) $oFC->page_content [ 'c044' ] = round($oFC->page_content [ 'c015' ] / $oFC->page_content [ 'c017' ],2);
	} else {
		$Lhulp06 = $Lhulp02 + $Lhulp04 - $Lhulp03;
		$Lhulp06 = log ($Lhulp06, 10);
		$Lhulp06 = 0.35004 * $Lhulp06;
		$Lhulp07 = log ($Lhulp01, 10);
		$Lhulp07 = 0.221 * $Lhulp07;
		$Lhulp08 = 1.29579 - $Lhulp06  + $Lhulp07;
		$oFC->page_content [ 'c041' ] = round (495 / $Lhulp08 - 450, 1);
		$LocalHulpT2 = sprintf ( "berekening 2 : vet pct =  %s / ( %s - %s * log ( %s + %s - %s ) + %s * log ( %s ) ) - %s",
			"495",
			"1.29579",
			"0.35077",
			$Lhulp02,
			$Lhulp04,
			$Lhulp03,
			"0.221",
			$Lhulp01,
			"450");
		if ($oFC->page_content [ 'c017' ] > 20) $oFC->page_content [ 'c044' ] = round ($oFC->page_content [ 'c015' ] / $oFC->page_content [ 'c017' ],2);
	}
} else {
	$oFC->page_content [ 'c041' ] = 0; // geen berekening op basis van vormfactor
}
	
if ( $oFC->page_content [ 'c041' ] == 0 || $oFC->page_content [ 'c040' ] == $oFC->page_content [ 'c041' ] ) {
	$oFC->page_content [ 'c042' ] = sprintf ( "%s" , $oFC->page_content [ 'c040' ] );
} else {
	$oFC->page_content [ 'c042' ] = sprintf ( "%s - %s" ,
		$oFC->page_content [ 'c041' ] < $oFC->page_content [ 'c040' ] ? $oFC->page_content [ 'c041' ] : $oFC->page_content [ 'c040' ],
		$oFC->page_content [ 'c041' ] > $oFC->page_content [ 'c040' ] ? $oFC->page_content [ 'c041' ] : $oFC->page_content [ 'c040' ] );
}
	
$Table [ '20' ] [ 'man'] = array (
	'8' => "Te laag",   
	'19.9' => "Normaal",   
	'25' => "Te hoog", 
	'100' => "Zeer hoog");
		
$Table [ '20' ] [ 'vrouw']= array (
	'21' => "Te laag",   
	'33.9' => "Normaal",   
	'39' => "Te hoog", 
	'100' => "Zeer hoog");
		
$Table [ '40' ] [ 'man'] = array (
	'11' => "Te laag",   
	'22.9' => "Normaal",   
	'28' => "Te hoog", 
	'100' => "Zeer hoog");
		
$Table [ '40' ] [ 'vrouw'] = array (
	'23' => "Te laag",   
	'34.9' => "Normaal",   
	'40' => "Te hoog", 
	'100' => "Zeer hoog");
		
$Table[ '60' ] [ 'man'] = array (
	'13' => "Te laag",   
	'24.9' => "Normaal",   
	'30' => "Te hoog", 
	'100' => "Zeer hoog");
		
$Table [ '60' ] [ 'vrouw'] = array (
	'24' => "Te laag",   
	'36.9' => "Normaal",   
	'42' => "Te hoog", 
	'100' => "Zeer hoog");
		
if ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) { $gender="man"; } else { $gender="vrouw"; }
$age="20";
if ( $oFC->page_content [ 'c030' ] > 39 ) $age="40";
if ( $oFC->page_content [ 'c030' ] > 59 ) $age="60";
$temp = $Table [ $age ] [ $gender ];
	
$oFC->page_content [ 'c043' ] = "";
$switch = -1;
foreach ( $temp as $key => $value ) {
	if ( $switch == 0 ) {
		$oFC->page_content [ 'c043' ] .= " - " . $key;
		$switch = 1;
	}
	if ( $switch < 0 ) {
		if ( strpos ( "normaal", strtolower ($value ) ) !== false ) {
			$oFC->page_content [ 'c043' ] = $key;
			$switch = 0;
}	}	}

if ( strpos ( "normaal", strtolower ($oFC->page_content [ 'c032' ] ) ) !== false) $oFC->page_content [ 'c098' ] = false;

	
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Lichaamsvet analyse</h4>" );
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Vet percentage : ", 
										$oFC->page_content [ 'c042' ],
										" pct " ); 
$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Bij u past een vet pct. van  : ", 
										$oFC->page_content [ 'c043' ],
										" pct " ); 										

if ( $oFC->page_content [ 'c044' ] > 0 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "MHR (Middel-Heup Ratio) : ", 
										$oFC->page_content [ 'c044' ],
										"" ); 	

$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Bij u past een MHR (Middel-Heup Ratio) van : ", 
										( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) ? "0,95" : "0,8",
										"" ); 												
															
if ( $oFC->setting [ 'debug' ] == "yes" ) {	
	
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Gender : ", 
											$oFC->page_content [ 'c009' ],
											"" ); 	
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Vet percentage : ", 
											$oFC->page_content [ 'c040' ],
											" pct " ); 

	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, $LocalHulpT1);
	
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Vet percentage : ", 
											$oFC->page_content [ 'c041' ],
											" pct " ); 
	
	if (isset ( $LocalHulpT2 ) )
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, $LocalHulpT2);
}

/* knijpkracht */

$Tableknijpman = array (  // 10 percentiel waarde
			'5' => 6,   
			'10' => 12,   
			'15' => 21, 
			'20' => 30,
			'25' => 36,
			'30' => 38,
			'35' => 39,
			'40' => 38,
			'45' => 36,
			'50' => 35,
			'55' => 34,
			'60' => 33,
			'65' => 31,
			'70' => 29,
			'75' => 26,
			'80' => 23,		
			'85' => 19,
			'90' => 16,
			'95' => 15 );
			
$Tableknijpvrouw = array (  // 10 percentiel waarde
			'5' => 6,   
			'10' => 12,   
			'15' => 17, 
			'20' => 21,
			'25' => 23,
			'30' => 24,
			'35' => 23,
			'40' => 23,
			'45' => 22,
			'50' => 21,
			'55' => 19,
			'60' => 18,
			'65' => 17,
			'70' => 16,
			'75' => 14,
			'80' => 13,		
			'85' => 11,
			'90' => 9,
			'95' => 8 );
			
if ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) {	
	$temp = $Tableknijpman;
} else {
	$temp = $Tableknijpvrouw;
}	

/* c050 dominantie handknijpkracht 
 * c051  handknijpkracht */ 
$oFC->page_content [ 'c050' ] = "";
$oFC->page_content [ 'c051' ] = 0;
$oFC->page_content [ 'c052' ] = 0;
$oFC->page_content [ 'c053' ] = '';
if ( $oFC->page_content [ 'c020' ] > 1 || $oFC->page_content [ 'c021' ] > 1 ) {
	if ( $oFC->page_content [ 'c020' ] > $oFC->page_content [ 'c021' ] ) {
		$oFC->page_content [ 'c050' ] = "LINKS";
		$oFC->page_content [ 'c051' ] = $oFC->page_content [ 'c020' ];	
	} else {
		$oFC->page_content [ 'c050' ] = "RECHTS";
		$oFC->page_content [ 'c051' ] = $oFC->page_content [ 'c021' ];
	}
	
		
	/* c052 10 percetiel 
	 * c053 qualificatie */	
	$oFC->page_content [ 'c052' ] = "";
	$switch = true;
	foreach ( $temp as $key => $value ) {
		if ( $switch ) {
			$oFC->page_content [ 'c052' ] = $value;
			if ( $key >= $oFC->page_content [ 'c030' ] ) $switch=false;
	}	}	 
	$oFC->page_content [ 'c053' ] = "";
	if ( $oFC->page_content [ 'c051' ] < $oFC->page_content [ 'c052' ] ) {
		$oFC->page_content [ 'c053' ] = "Te laag";
	}

	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Handknijpkracht analyse</h4>" );
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Dominantie : ", 
											$oFC->page_content [ 'c050' ],
											"" ); 
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Handknijpkracht : ", 
											$oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'c051' ] , "s{ WHOLE|STRONG }" ) . " kg.",
											$oFC->page_content [ 'c053' ] );
											
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Bij u past een handknijpkracht van minstens : ", 
											$oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'c052' ] , "s{ WHOLE }" )  . " kg.",
											"" );	

	if ( $oFC->setting [ 'debug' ] == "yes" ) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");

		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "handknijpkracht tabel gebruikt (10 percetiel)" );
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Gender : ", 
												$oFC->page_content [ 'c009' ],
												"" ); 
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Leeftijd : ", 
										$oFC->page_content [ 'c030' ],
										" jaar" ); 	
		foreach ($temp as $key => $value ) { 
			$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "leeftijd : ".$key, $value , ""  );
}	}	}

$Tablebeweging = array ( 
	'a0' => 'Geen of weinig beweging', 
	'a1' => '1-3 keer per week oefeningen', 
	'a2' => '4-5 keer per week oefeningen', 
	'a3' => 'Dagelijks oefeningen of 3-4 keer per week sport', 
	'a4' => '6-7 keer per week sport', 
	'a5' => 'Dagelijks flinke training of zwaar lichamelijk werk');
	
$Tablebeweging2 = array ( 
	'a0' => '1.2', 
	'a1' => '1.375', 
	'a2' => '1.465', 
	'a3' => '1.55', 
	'a4' => '1,725', 
	'a5' => '1.9');
		
/* c050 BMR  
 * c051 calorie verbruik  */
$Lhulp01 = $oFC->page_content [ 'c010' ] ; // gewicht
$Lhulp02 = $oFC->page_content [ 'c011' ] ; // lengte
$Lhulp03 = $oFC->page_content [ 'c030' ] ; // leeftijd
$Lhulp04 = $oFC->page_content [ 'c041' ] ; // vet %
$Lhulp05 = $Tablebeweging2 [ $oFC->page_content [ 'c025'] ] ; // activiteit level
$Lhulp06 = ( 10 * $Lhulp01 ) + ( 6.25 * $Lhulp02 ) - ( 5 *  $Lhulp03 );

	
if ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) {	
	$oFC->page_content [ 'c054' ] = round ( $Lhulp06 , 0) + 5;
	$oFC->page_content [ 'c055' ] = round ( $oFC->page_content [ 'c054' ] * $Lhulp05 , 0);
} else {
	$oFC->page_content [ 'c054' ] = round ( $Lhulp06 , 0) - 161;
	$oFC->page_content [ 'c055' ] = round ( $oFC->page_content [ 'c054' ] * $Lhulp05 , 0);
}
	
if ( $oFC->page_content [ 'c099' ] ) {	
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Calorie gebruik berekeningen</h4>" );
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "BMR (Calorie verbruik in complete rust) : ", 
											$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c054' ] , "s{ WHOLE }" ) ,
											" kcal" ); 
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, $oFC->language [ 'beweging' ] [ $oFC->page_content [ 'c025' ] ], 
											$Tablebeweging2 [ $oFC->page_content [ 'c025'] ] . " x ",
											"" ); 										
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Calorie behoefte bij activiteiten level : ", 
											$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c055' ] , "s{ WHOLE }" ) ,
											" kcal" ); 
	
	if ( $oFC->setting [ 'debug' ] == "yes" ) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Gender : ", 
												$oFC->page_content [ 'c009' ],
												"" ); 
		if ( strlen ($oFC->page_content [ 'c009' ] ) < 4 ) {	
			$LocalHulpT3 = sprintf ( "berekening BMR : ( %s * %s ) + ( %s * %s ) - ( %s * %s ) + %s;",
			"10",
			$Lhulp01,
			"6.25",
			$Lhulp02,
			"5",
			$Lhulp03,
			"5");
		} else {
			$LocalHulpT3 = sprintf ( "berekening BMR : ( %s * %s ) + ( %s * %s ) - ( %s * %s ) 1 %s;",
			"10",
			$Lhulp01,
			"6.25",
			$Lhulp02,
			"5",
			$Lhulp03,
			"161");
		}
		$LocalHulpT4 = sprintf ( "Calorie berekening : %s ( = berekening BMR ) * %s",
					$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'c050' ] , "s{ WHOLE }" ),
					$Tablebeweging2 [ $oFC->page_content [ 'c025'] ] );
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, $LocalHulpT3); 
		
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "Tabel gebruikt " );
		foreach ($Tablebeweging as $key => $value ) { 
			$payload = $Tablebeweging2 [ $key ];
			$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, $value, $payload , ""  );
		}
		$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, $LocalHulpT4); 
	}								
}

if ( $oFC->page_content [ 'z101' ] ) {	
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE0, "");
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE6, "<h4>Extra Parameters</h4>" );
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Naam : ", 
											$oFC->page_content [ 'z101' ] ,
											"" ); 
	$oFC->page_content [ 'RAPPORTAGE' ] .= sprintf ( $TEMPLATE2, "Parameter : ", 
											$oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'z102' ] , "s{ KOM1 }" ),
											"" ); 		
}

$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6"> </td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '</table>';

?>