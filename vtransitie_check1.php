<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

//check values
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content,   $oFC->language ['trans'] ), __LINE__ . __FUNCTION__ ); 

$oFC->page_content [ 'c001' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'c001' ] ?? "0", "v{". $oFC->language ['trans'][ 'c001' ].";0;100000}" );	
$oFC->page_content [ 'b030' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'b030' ] ?? "0", "v{". $oFC->language ['trans'][ 'c002' ].";0;20000}" );	

$oFC->page_content [ 'b001' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'b001' ] ?? "--", "s{ TOASC|CLEAN|TRIM } " );
$oFC->page_content [ 'b010' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'b010' ] ?? "--", "s{ TOASC|CLEAN|TRIM } " );

$oFC->page_content [ 'b002' ] = $oFC->gsm_sanitizeStringD ( $oFC->page_content [ 'b002' ] ?? "0", "y{". $oFC->language ['trans'][ 'b002' ]. ";" . $oFC->language ['trans'][ 'b002' ] . ";" . $oFC->page_content [ 'DATE' ] ."}" );
$oFC->page_content [ 'b020' ] = $oFC->gsm_sanitizeStringD ( $oFC->page_content [ 'b020' ] ?? "0", "y{". $oFC->page_content [ 'DATE' ] .";1970-01-01;". $oFC->page_content [ 'DATE' ] ."}" );	
$oFC->page_content [ 'b021' ] = $oFC->gsm_sanitizeStringD ( $oFC->page_content [ 'b021' ] ?? "0", "y{". $oFC->page_content [ 'DATE' ] . ";" . $oFC->page_content [ 'b020' ] . ";" . $oFC->page_content [ 'DATEHIGH' ]  . "}" );

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content,   $oFC->language ['trans'] ), __LINE__ . __FUNCTION__ ); 
