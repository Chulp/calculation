<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/ 
$module_name = 'xcalculation';
$version = ' v20240426';
$project = "Calculation base";
$default_prefix = "cal_";
$proces_defaults_at = sprintf ("%sjson_defaults.html", $default_prefix) ;
$default_template = sprintf ("/%sform.lte", $default_prefix) ;

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffc::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET , "droplet" );

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;
$oFC->page_content [ 'DEBUG' ] = ($oFC->setting [ 'debug' ] == "yes" ) ? true : false;

$oFC->page_content [ 'DATEHIGH' ] = ( date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m" ) + 24, '01', date ( "Y" ) ) ) );
$oFC->page_content [ 'PDF_LOCATION' ] = sprintf ( "%s/%s/XX_%s", $oFC->setting [ 'collectdir' ], date ("Y_m_d"), session_id ( ) );
$oFC->page_content [ 'GEBRUIK' ] = '';

/* get procesdefaults 1 */
$oFC->page_content = array_merge ( $oFC->page_content, $oFC->language [ 'cal' ] );

/* get procesdefaults 2 */
$LocalHulp = $oFC->setting [ 'frontend' ] . $proces_defaults_at;
if ( file_exists ( $LocalHulp ) ) {
	$LocalHulp1 = file_get_contents ( $LocalHulp );	
	$oFC->page_content = array_merge ( $oFC->page_content, json_decode ( $LocalHulp1 , true ) );
}

/* get procesdefaults 3z */
$LocalHulp = $oFC->setting [ 'frontend' ] . $proces_defaults_at;
if ( file_exists ( $LocalHulp ) ) {
	$LocalHulp1 = file_get_contents ( $LocalHulp );	
	$oFC->page_content = array_merge ( $oFC->page_content, json_decode ( $LocalHulp1 , true ) );
}

/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* get memory values */ 
$oFC->gsm_memorySaved ( );

/* get memory values */
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC, $selection ?? ""), __LINE__ . __FUNCTION__ ); 

/* selection * /
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}	

/* sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
		$oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* reset before job */ 
if ( isset( $_POST [ 'command' ] ) && $_POST [ 'command' ] == "Reset" ) { 
	unset ( $_POST ); 
	unset ( $_GET ); 
	$oFC->description .= date ( 'G:i:s' . substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
		$oFC->language [ 'TXT_REMOVE_INPUT' ] . NL; 
}

/*
 * which job to do
 */
$oFC->page_content [ 'P1' ] = false; // first cycle ?
if ( isset( $_POST[ 'command' ] ) ) {
	$oFC->page_content [ 'P1' ] = false; // niet eerste cycle 
	foreach ($_POST as $pay =>$load ) {	
		if (substr ( $pay, 0, 4 ) == "gsm_" ) {
			$oFC->page_content [ substr ( $pay, 4 ) ] = strip_tags ( $load );	
	}	}
	/* externe checks */ 
	$check = $oFC->setting [ 'includes' ] . $module_name . '_check' . '1' . '.php';
	$checkx = $oFC->setting [ 'includes' ] . "v" . substr ($module_name, 1) . '_check' . '1' . '.php';
	if ( file_exists ( $check ) ) { 
		require_once ( $check ); 	
	} elseif  ( file_exists ( $checkx ) ) { 
		require_once ( $checkx ); 
	}

	/* jobs */ 
	switch ( $_POST[ 'command' ] ) {
		
		case "Reset":
			break;
			
		case "Print":
			$oFC->setting [ 'pdf_filename' ] = $oFC->gsm_sanitizeStrings ( sprintf ( "Calculation_%s_%s.pdf", 
				date ( "Ymd", time ( ) ), 
				$oFC->page_content [ 'c001' ] ), 
				"s{FILE}" );
			break;
			
		case "Save":
			$to = sprintf ( "%s%s/logging/%s/XX_%s/", LEPTON_PATH, MEDIA_DIRECTORY, date ( "Y_m_d"),session_id());
			$save_it = array ( "c","x","y","z");
			$temp = array();
			foreach ($oFC->page_content as $key => $value) {
				if ( in_array ( substr ( $key, 0, 1 ), $save_it ) ) $temp [ $key ] = $value;
			}
			if ( count ( $temp ) >0 ) {
				$file =  $oFC->gsm_sanitizeStrings ( sprintf ( "%sx%s.html", $default_prefix , $temp [ "c002" ]) , "s{FILE}" );
				$payload = json_encode ( $temp );

				$file = sprintf ( "%s%s", $to, $file); 
				if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ($file, "post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC->page_content ), __LINE__ . __FUNCTION__ ); 

				file_put_contents ( $file, $payload );
			}
			
			$to = $oFC->setting [ 'frontend' ] ;
			$temp = array();
			foreach ($oFC->page_content as $key => $value) {
				if ( substr ( $key, 0, 1) ==  "z" )  $temp [ $key ] = $value;
			}
			if ( count ( $temp ) >0 ) {
				$file = $oFC->gsm_sanitizeStrings ( sprintf ( "%sz%s.html", $default_prefix, $temp [ "z101" ]), "s{FILE}" );
				$payload = json_encode ( $temp );
				$file = sprintf ( "%s%s", $to, $file); 
				if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ($file, "post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC->page_content ), __LINE__ . __FUNCTION__ ); 
				file_put_contents ( $file, $payload );
			}
			break;
			
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {	
	
		case "Delete":
			$localhulpB = str_replace (  LEPTON_URL, LEPTON_PATH, $_GET[ 'recid' ]);
			if ( file_exists ( $localhulpB ) ) unlink ( $localhulpB );
			$oFC->page_content [ 'P1' ] = true; // eerste cycle aangenomen
			$oFC->page_content [ 'MODE' ] = 9;
			break;
			
		case "Select":
			$LocalHulp = str_replace (  LEPTON_URL, LEPTON_PATH, $_GET[ 'recid' ] );
			if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $LocalHulp ), __LINE__ . __FUNCTION__ ); 
			if ( file_exists ( $LocalHulp ) ) {
				$LocalHulp1 = file_get_contents ( $LocalHulp );	
				$oFC->page_content = array_merge ( $oFC->page_content, json_decode ( $LocalHulp1 , true ) );
			}
			break;
			
		default:
			// escape route 
			$oFC->page_content [ 'P1' ] = true; // eerste cycle aangenomen
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else {
	$oFC->page_content [ 'MODE' ] = 9;
	$oFC->page_content [ 'P1' ] = true; // eerste cycle 
	$oFC->page_content [ 'c001' ] = "P" . date('U');
	$oFC->gsm_existDir ( $oFC->page_content [ 'PDF_LOCATION' ] , true  );
	if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array( $oFC->page_content ), __LINE__ . __FUNCTION__ ); 
}

	/* calculatie */
if ( !$oFC->page_content [ 'P1' ] ) {
	$check = $oFC->setting [ 'includes' ] . $module_name . '_check' . '2' . $oFC->page_content [ 'GEBRUIK' ] . '.php';
	$checkx = $oFC->setting [ 'includes' ] . "v" . substr ( $module_name, 1 ) . '_check' . '2' . $oFC->page_content [ 'GEBRUIK' ] . '.php';
	if ( file_exists ( $check  ) ) { 
		require_once ( $check ); 
	} elseif ( file_exists ( $checkx  ) ) { 
		require_once ( $checkx ); 
	}

	/* opmaak voor de uitvoer : de filenaam */
	$oFC->setting [ 'pdf_filename' ] = $oFC->gsm_sanitizeStrings ( sprintf ( "calculation_%s_%s_%s.pdf", 
		date ( "Ymd", time ( ) ), 
		$oFC->page_content [ 'c001' ], 
		$oFC->page_content [ 'c003' ] ), "s{FILE}" );
	/*  de print */
	$oFC->gsm_print ( "", 
			$project, "" , 
			"1" . $oFC->page_content [ 'GEBRUIK' ], 
			$oFC->page_content [ 'PDF_LOCATION' ]  );	
	/*  einde uitvoer */
}

/*
 * the selection options
 */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	case 9:
	default: 
		$oFC->page_content [ 'SELECTIONA' ] = array();
		if ( $oFC->page_content [ 'P1' ] ) {
			$from = sprintf ( "%s%s/logging/%s/XX_%s/", LEPTON_PATH, MEDIA_DIRECTORY, date ( "Y_m_d"),session_id());
			if ( is_dir ( $from ) ) {
				$ext1 = $default_prefix."x*.html";
				$all = glob("$from$ext1", GLOB_MARK);
				foreach ($all as $a) { 
					$localHulpA = basename( $a );
					if ( !is_dir ( $a ) ) {
						$ff = array ( "key" => str_replace ( LEPTON_PATH, LEPTON_URL, $from ) . $localHulpA,
							"label" => str_replace ( ".html", "", substr ($localHulpA, 5 ) ) );
						$oFC->page_content [ 'SELECTIONA' ][] = $ff ;
		}	}	}	}
		
		$from = $oFC->setting [ 'frontend' ];
		if ( is_dir ( $from ) ) {
			$temp= array ();
			$temp1 = array ();
			$ext1 = $default_prefix."z*.html";
			$all = glob("$from$ext1", GLOB_MARK);
			foreach ($all as $a) { 
				$localHulpA = basename( $a );
				if ( !is_dir ( $a ) ) {
					$temp1 [ $localHulpA ] = str_replace ( ".html", "", substr ($localHulpA, 5 ) );
					$ff = array ( "key" => str_replace ( LEPTON_PATH, LEPTON_URL, $from ) . $localHulpA,
						"label" => str_replace ( ".html", "", substr ($localHulpA, 5 ) ) );	
					$temp[] = $ff ;
			}	}	
			$oFC->page_content [ 'z100S' ] = $oFC->gsm_selectOption ( $temp1, $oFC->page_content [ 'z101' ] ?? "-" );
		}

		if ( $oFC->page_content [ 'P1' ] ) {
			$oFC->page_content [ 'SELECTIONC' ] = "";
		} else {
			$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 11 ), 
				"-", 
				"/" . $oFC->setting [ 'pdf_filename' ], 
				"-", "-", "-", "-", 
				$oFC->page_content [ 'PDF_LOCATION' ] );
		}		
		$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 2) );
		$oFC->page_content [ 'SELECTIOND' ] = $oFC->gsm_opmaakSel ( array ( 6, 7) );		
		break; 
}

/*
 * the output to the screen
 */
if ( $oFC->page_content [ 'P1' ] ) { 
	$oFC->page_content [ 'REFERENCE_ACTIVE1'] = 'active'; 
	$oFC->page_content [ 'REFERENCE_ACTIVE9'] = ''; 
	
} else {
	$oFC->page_content [ 'REFERENCE_ACTIVE1'] = ''; 
	$oFC->page_content [ 'REFERENCE_ACTIVE9'] = 'active'; 
}

$oFC->page_content [ 'REFERENCE_ACTIVE0'] = ''; 
if ( !$oFC->page_content [ 'P1' ] || count ( $oFC->page_content [ 'SELECTIONA' ] ) >0  ) {
	$oFC->page_content [ 'REFERENCE_ACTIVE0'] = '';
} else {
	$oFC->page_content [ 'REFERENCE_ACTIVE0'] = 'will not be shown';
}

/* memory save */
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( 2 ); 

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: //list
		break;
} 

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>