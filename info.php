<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$module_directory 	= 'gsmoffc';
$module_name 		= 'Calculate';
$module_function 	= 'page';
$module_version 	= '1.2.1';
$module_platform 	= '7.0.0';
$module_date 		= '20250115';
$module_platform	= 'Lepton 7.0.0'; //tested on this platform, 
$module_author 		= '<a href="http://www.contracthulp.nl" target="_blank">Gerard Smelt/ContractHulp</a>';
$module_license     = '<a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License 3.0</a>';
$module_license_terms = '-';
$module_guid 		= 'B4156004-F43D-4F9C-9C07-42C700A56704';
$module_description = 'This module provides miscelaneous calculations';
$module_home 		= 'http://www.contracthulp.nl';

/* guid via UUID-GUID Generator Portable 1.1. */
/* after first installation start backend with d_repair_ to initialise tables*/
?>
