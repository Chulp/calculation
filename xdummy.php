<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

/* module id */  
$module_name = 'xdummy';
$version='20240204';
$project="dummy";
$default_template = '/display.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffc::getInstance();
$oTWIG = lib_twig_box::getInstance();
$oTWIG-> registerModule( LOAD_MODULE. LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];

/* file references */

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC , $selection ), __LINE__ . $module_name );  

/* Input processing */
if ( isset( $_POST[ 'command' ] ) ) {
  switch ( $_POST[ 'command' ] ) {
    default:
    // escape route
      $oFC->page_content [ 'MODE' ] = 9;
      break;
  } 
} elseif ( isset( $_GET[ 'command' ] ) ) {
  switch ( $_GET[ 'command' ] ) {
    default:
    // escape route 
      $oFC->page_content [ 'MODE' ] = 9;
      break;
  } 
} else {
   // so standard display / first run
    $oFC->page_content [ 'MODE' ] = 9;
}

/* display processing */
switch (  $oFC->page_content [ 'MODE' ] ) {
  default: // default list   
	$oFC->page_content['TOEGIFT'] = ''; 
	foreach ($oFC->language [ 'DUMMY' ] as $pay => $load ) $oFC->page_content['TOEGIFT'] .=  $load . NL; 
    break;
} 

/* selection options */
switch (  $oFC->page_content [ 'MODE' ] ) {
  default: // default list 
	foreach ( $oFC->version as $key => $value) $oFC->description .= $key . "=>" . $value.NL; 
	$oFC->page_content [ 'SELECTION' ] = "";
    break;
} 
/* output processing */

$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
$oFC->page_content [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id;   

/* output handling */
switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
		break;
}

echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>