<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
// function to remove old directories
function gsm_rmdir( $dir )	{
	if (empty($dir)) return false;
	if ( is_dir( $dir ) )  {
 		$files = scandir( $dir );
  			foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) gsm_rmdir( "$dir/$file" ); }
    		rmdir( $dir );
 	} else if ( file_exists( $dir ) ) {
    	unlink( $dir );
}	}

// functions to copy files and non-empty directories  
function gsm_xcopy( $src, $dst ) {
	// 	if ( file_exists( $dst ) ) { gsm_xrmdir( $dst ); }
 	if ( is_dir( $src ) )  {
   		if ( !file_exists( $dst ) ) mkdir( $dst, 0777);
    	$files = scandir( $src );
    	foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) xcopy( "$src/$file", "$dst/$file" ); }
  	} else if ( file_exists( $src ) ) { copy( $src, $dst );}	
} 
 
if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) {  
	if (strstr($xmode, "DETAIL")) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Detail function is selected'.NL;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' This module version  : 20240207';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' selection :'. $xmode;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Functions detected: ';
		
		if (strstr ( $xmode, "DETAIL" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' DETAIL.';
		if (strstr ( $xmode, "IMAGE" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' IMAGE.';
		if (strstr ( $xmode, "INSTAL" ) )	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' INSTALL.';
		if (strstr ( $xmode, "LOGGING" ) ) 	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' LOGGING.';
		if (strstr ( $xmode, "EXPL" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' EXPL.';
		
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' company :'. $oFC->setting [ 'droplet' ] [ LANGUAGE . '0' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' webmaster : '. $oFC->setting [ 'droplet' ] [ LANGUAGE . '4' ] ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' mailadres : '. $oFC->setting [ 'droplet' ] [ LANGUAGE . '10' ] ;
		
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Directories involved :';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'collectdir' ] ?? "--" ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'datadir' ] ?? "--" ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'mediadir' ] ?? "--" ;
		foreach ( $oFC->setting [ 'entity' ] as $key => $value ) 
			$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' entity : '. $key . " | " .$value ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' owner :'. $oFC->setting [ 'owner' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' allowed :'. implode("|", $oFC->setting [ 'allowed' ] ) ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' menu :'. $FC_SET [ 'SET_menu' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' function :'. $FC_SET [ 'SET_function' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' aantal regels : ' . $oFC->setting [ 'qty_max' ]  ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Tables involved :';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' end settings :'.NL;
	} 
	
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ( "this"=>$oFC ), __LINE__ . "repair" ); 


/* install */
	if (strstr ( $xmode, "INSTAL" ) ) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start Install function: Frontend for this module will be created / re-created';
		// determine directories to be used.
		$dir_from = LEPTON_PATH.'/modules/'.LOAD_MODULE.LOAD_SUFFIX.'/frontend/' .LOAD_MODULE.LOAD_SUFFIX;
		$dir_to = LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/'.LOAD_MODULE.LOAD_SUFFIX;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Frontend files (from install package) will be moved to default template.';
		// copy and replace function
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' from : '.$dir_from;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' to : '.$dir_to;
		if ( file_exists ( $dir_to ) ) {
			$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' save previous to  : '.$dir_to."_bck";
			$oFC->gsm_copyFile( $dir_to, 1, $dir_to."_bck" );
		}
		$oFC->gsm_copyFile( $dir_from, 1, $dir_to );
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' End Install function'.NL;
	} 	
		
/* images */		
	if (strstr ( $xmode, "IMAGE" ) ) {	
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start Image function';
		$dir_from = LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/img/';
		$dir_to = LEPTON_PATH.'/modules/'.LOAD_MODULE.LOAD_SUFFIX.'/img/';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Frontend image directory will be moved to image directory (to overwrite install package).';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' from : '.$dir_from;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' to : '.$dir_to;
		$oFC->gsm_copyFile( $dir_from, 3, $dir_to );
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' End Image function'.NL;
	}  

/* remove data * /
	if (strstr($xmode, "REMOVE")) {
		$nremoved=0;
		$job=array();
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start remove function' . NL;
		$check_query = "SELECT * FROM `" . $oFC->file_ref [ 99 ]."`  ORDER BY `area`";
		$results = array();
		if ( $database->execute_query( $check_query, true, $results) && count($results) == 0) $oFC->description .= $oFC->language [ 'TXT_ERROR_DATA' ]. NL; 
		foreach ($results as $result) { 
			if ( ( $result [ 'date' ] > $result [ 'keeptill' ] && strpos ( $xmode, 'EXPIRE' ) !== false ) || 
				( strpos ( $result [ 'ref' ], $oFC->language [ 'TXT_REMOVE_REF' ] ) !== false || 
				strpos ( $result [ 'keywords' ], $oFC->language [ 'TXT_REMOVE_KEYWORD' ] ) !== false ) ) {
				$path = $result [ 'area' ] . $result [ 'location' ]. $result [ 'name' ]; 
				$location = LEPTON_PATH . $path;  
				$job [] = "DELETE FROM `" . $oFC->file_ref [ 99 ]."` WHERE `id` = ". $result [ 'id' ];
				if ( file_exists($location ) ) {
					unlink ( $location);
					$nremoved++;
					$oFC->description .= $nremoved . ' : '. $path . NL;
		}	}	}
		// actual change;
		$n=0;
		if ( isset ( $job ) && count( $job ) > 0 ) {
			foreach( $job as $key => $query ) {
				$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' .$query.NL;
				$database->simple_query ( $query);
				$n++;
		} 	}
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Database entries removed : '.$n.NL; 			
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Documents removed : '.$nremoved.NL; 	 
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' End remove function' . NL;
	} 

/*repair database * /
	if (strstr($xmode, "REPAIR")) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start repair function' . NL;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Copy attempt existing database' . NL;
		$job[1] = "DROP TABLE IF EXISTS `" . $oFC->file_ref [ 99 ] . "_cpy`";
		$job[2] = "RENAME TABLE `" . $oFC->file_ref [ 99 ] . "` TO `" . $oFC->file_ref [ 99 ] . "_cpy`";
		$job[3] = "CREATE TABLE `" . $oFC->file_ref [ 99 ] . "` LIKE `" . $oFC->file_ref [ 99 ] . "_cpy`";		
		$results = $database->simple_query ( $job [ 1 ] );
		$results = $database->simple_query ( $job [ 2 ] );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Create new database' . NL;
		$results = $database->simple_query ( $job [ 3 ] );
		$oFC->gsm_existDir ( sprintf ( '%s%s' ,LEPTON_PATH, $oFC->setting [ 'datadir' ]), true  );
		
		// copy data per region regions
		$preloadArr = array( );
		$check_results = array();
		$database->execute_query ( 
			"SELECT `area` FROM `" . $oFC->file_ref [ 99 ] . "_cpy`  ORDER BY `area`", 
			true, 
			$check_results);
		if ( count($check_results) == 0) $oFC->description .= 'Existing database was empty'. NL; 
		foreach ( $check_results as $result ) {
			if ( strlen ( $result [ 'area' ] ) > 2) $preloadArr [ $result[ 'area' ] ] = $result [ 'area' ];
		}
		$preloadArr [ $oFC->setting [ 'datadir' ] ] = $oFC->setting [ 'datadir' ];
		
		// create database content
		foreach  ( $preloadArr as $key=>$value )  {
			$oFC->description .= "Load " . $key . " area" . NL;
			$oFC->description .= $oFC->gsm_recreateDatabase ( $key, $xmode ).NL;
		}
		$xmode .= "TRANSFER" ;
		$xmode .= "CHECK" ;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Database repair is initiated'.NL; 
		$oFC->description .= NL.date ( "H:i:s " ) . __LINE__  . ' End repair function' . NL;
	} 

/* keywords castor specifiek * /
	if (strstr($xmode, "xKEYWORD")) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start keyword function' . NL;
		$oFC->description .= $oFC->castor_Keywords ($oFC->datadir, $xmode);
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' End keyword function' . NL;
	}

/* equal castor specific * /
	if (strstr($xmode, "xEQUAL")) {
		$nremoved=0;
		$job=array();
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start checking equal (size) function' . NL;
		$check_query = "SELECT * FROM `" . $oFC->file_ref [ 99 ]."`  ORDER BY `docsize`";
		$results = array();
		if ( $database->execute_query ( $check_query, true, $results) && count($results) == 0) $oFC->description .= $oFC->language ['TXT_ERROR_DATA']. NL; 
		$LevelName = "";
		$LevelDocsize = 0;
		foreach ($results as $result) { 
			if ( $result['docsize'] == $LevelDocsize ) {
				//report
				$oFC->description .= NL . sprintf( "docsize %s : %s", $LevelDocsize, $LevelName) . NL;
				$oFC->description .= sprintf( "docsize %s : %s", $result['docsize'], $result['name']) . NL;
				$nremoved++;
			}
			$LevelDocsize = $result['docsize'];
			$LevelName = $result['name'];
		}
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Documents suspected equal : '.$nremoved.NL;
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . ' End equal function' . NL;
	} 
	

/* take over previous parameters * /
	if ( strstr ( $xmode, "TRANSFER" ) ) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start transfer function' . NL;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Check database tables' . NL;
		$TEMPLATE = "SHOW TABLE STATUS LIKE '%s'";
		$check_results = array ();
		$job = array();
		$database->execute_query( 
			sprintf ( $TEMPLATE, $oFC->file_ref [ 99 ] . "_cpy" ), 
			true, $check_results);
		if (count( $check_results) > 0) { // existing
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' History found'.NL; 
			$check_results = array ();
			$database->execute_query ( 
				"SELECT * FROM `" . $oFC->file_ref [ 99 ] . "` ORDER BY `updated` ASC", 
				true, 
				$check_results );
			foreach ($check_results as $result) {
				$history_query = "SELECT * FROM `" . $oFC->file_ref [ 99 ] . "_cpy` WHERE `name` = '" . $result [ 'name' ] . "'";
				$history_results = array();
				if ( $database->execute_query( $history_query, true, $history_results) && count( $history_results) > 0) { 
					$row = current ( $history_results );
					$updateArr = array();
					if ($row [ 'content_short' ] != $result [ 'content_short' ] ) $updateArr [ 'content_short'] = str_replace ("'", " ",$row [ 'content_short' ]); 
					if ( count ( $updateArr ) > 0 ) 
						$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . 
						$oFC->gsm_accessSql ( $updateArr, 2 ) . " WHERE `id` = '" . $result [ 'id' ] . "'"; 
				}
			}
			if ( isset ( $job ) && count ( $job ) > 0 ) {
				foreach ( $job as $key => $query ) {
					$database->simple_query ( $query ) ;
					$oFC->description .= date ( "H:i:s " ) . __LINE__ . " " . $query.NL;
				}
				$oFC->description .= date ( "H:i:s " ) . __LINE__  . " Aantal records aangepast : ". count ( $job ). NL;
			}
		} else {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' No History found'.NL; 
		}
		$oFC->description .= NL.date ( "H:i:s " ) . __LINE__  . ' End transfer function' . NL;
	} 

/* check and reallocate * /
	if ( strstr ( $xmode, "CHECK" ) ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Check and reallocate' . NL;
		$fields = array( "area", "location", "name", "type", "hash", "id");
		$check_results = array();
		$database->execute_query( 
			"SELECT `" . implode ("`, `", $fields) . "` FROM `" . $oFC->file_ref [ 99 ]."`  ORDER BY `type`", 
			true, 
			$check_results); 
		if( count( $check_results) == 0) $oFC->description .= 'New databse is empty'. NL; 
		$nremoved=0;
		$nmoved=0;
		$job = array();
		foreach ($check_results as $result) {
			if ($result['type'] != substr($result['location'],1,2)) {
				$location_n = sprintf ("/%s/%s/", $result['type'], substr($result['hash'],0,2));
				$location_new = LEPTON_PATH. $result['area']. $location_n. $result['name']; 
				$location = LEPTON_PATH. $result['area']. $result['location']. $result['name']; 
				if ( file_exists ( $location ) ) {
					$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " unexpected location ".NL.$location.NL;
					$dir_to= LEPTON_PATH. $result['area']."/".$result['type']."/".substr($result['hash'],0,2);
					$oFC->gsm_existDir ( $dir_to );
					$oFC->description .= date ( "H:i:s " ) . __LINE__  . " target location ".$location_new.NL;
					if ( !file_exists ( $location_new ) ) {
						rename ($location, $location_new);
						$nmoved++;
						$oFC->description .= $nmoved . ' moved : '. $result ['name' ] . NL;
						$updateArr = array();
						$updateArr ['location'] = $location_n;
						if ( count ( $updateArr ) > 0 ) 
							$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . 
							$oFC->gsm_accessSql ( $updateArr, 2 ) . " WHERE `id` = '" . $result [ 'id' ] . "'"; 
					}
				} else {
					$oFC->description .= date ( "H:i:s " ) . __LINE__  . " file not found " . NL . $location . NL;
					$updateArr = array();
					$updateArr ['location'] = $location_n;
					if ( count ( $updateArr ) > 0 ) 
						$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . 
						$oFC->gsm_accessSql ( $updateArr, 2 ) . " WHERE `id` = '" . $result [ 'id' ] . "'"; 
				}
			}
		}
		if ( isset ( $job ) && count ( $job ) > 0 ) {
			foreach ( $job as $key => $query ) $database->simple_query ( $query ) ;
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . " Records modifed : ". count ( $job ). NL;
		}
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' remove empty directories' . NL;
		$oFC->gsm_removeEmptyFolders ();
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' End check and reallocate function' . NL;
	}
	
/* remove logging */
	if ( strstr ( $xmode, "LOG" ) ) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start logging function' . NL;
		$dir_to_remove = sprintf ( "%s%s/" , LEPTON_PATH, $oFC->setting [ 'collectdir' ] ?? "/geen data/"  );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' remove attempt ' .$dir_to_remove. NL;
		if ( file_exists ( $dir_to_remove ) ) {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' remove ' . $dir_to_remove . NL;
			gsm_rmdir ( $dir_to_remove );
		}
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' create empty directory' . NL;
		if ( !file_exists ( $dir_to_remove ) ) mkdir ( $dir_to_remove, 0777 );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' empty directry creation completed' . NL;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' End logging function' . NL;
	} 

	if ( strstr ( $xmode, "EXPL" ) ) {
		$oFC->page_content [ 'TOEGIFT' ] .= '<table class="ui very basic collapsing celled table">';
		$oFC->page_content [ 'TOEGIFT' ] .= '<tr class="active"><td><strong>d_xxxx_</strong></td><td><strong>format</strong></td></tr>';
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>DETAIL </td><td> detailed data</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>LOGGING </td><td> empty the logging</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>INSTALL </td><td> frontend files are created: customized values are overwritten bij default values !!</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>IMAGE </td><td> image directory copied from frontend</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "</table>";		
	}

}
?>
