<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

 
/* debug * / Gsm_debug ( array ( "field" => $this->page_content) , __LINE__ . __FUNCTION__ . " print start" );/* debug */ 

$pdf = false;
if ( isset ( $this->setting [ 'pdf_filename' ] ) && strlen ( $this->setting [ 'pdf_filename' ] ) > 10 ) {
	$pdf = true;
	require_once ( $this->setting [ 'includes' ] . 'classes/pdf.inc' );
	$pdf = new PDF();
	global $owner;
	$owner = $this->setting [ 'owner' ];
	global $title;
	$title = "Antropometrie overzicht"; //$project;
	$pdf->AliasNbPages();
	$z=1;
	$pdf->AddPage();
	$pdf_text   = '';
	$ral = array();
	$pdf_cols = array( 4, 70, 10, 22, 68, 0); 
	/* naam */
	$pdf_text .= $this->page_content [ 'a001' ] ; 
	/* leeftijd */
	if ( $this->page_content [ 'a002' ] == $this->language [ 'antropo' ][ 'z002' ] || $this->page_content [ 'a030' ] < 20 )	{
		$pdf_text .=  "\n";
	} else {	
		$pdf_text .= sprintf ( " ( %s )", $this->page_content [ 'a030' ] );	
		$pdf_text .=  "\n"; 
	}
	/* leeftijd */	
	$pdf->ChapterXLarge ( $pdf_text );
	$pdf_text   = '';
}

if ($pdf ) {
	$pdf_text .= "\n".'Ingevoerde gegevens'. "\n";
	$pdf->ChapterLarge( $pdf_text );
	$pdf_text = "";
	if ( $this->page_content [ 'a002' ] != $this->language [ 'antropo' ][ 'z002' ] )	
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Geboortedatum : ", "",$this->gsm_sanitizeStrings ( $this->page_content [ 'a002' ], "s{ DATUM }" ),   "", "" ) ) );
	$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Geslacht : ", "", $this->page_content [ 'a009' ],   "", "" ) ) );
	if ( $this->page_content [ 'a020' ] > 1 )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Gewicht : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a020' ] , "s{ KOM1 }" ) . " kg" ,   "", "" ) ) );
	if ( $this->page_content [ 'a021' ] > 1 )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Lengte : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a021' ] , "s{ KOM1 }" ) . " cm" ,   "", "" ) ) );
	if ( $this->page_content [ 'a023' ] > 1 )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Nekwijdte : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a023' ] , "s{ KOM1 }" ) . " cm" ,   "", "" ) ) );
	if ( $this->page_content [ 'a022' ] > 1 )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Buikomvang : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a022' ] , "s{ KOM1 }" ) . " cm" ,   "", "" ) ) );
	if ( $this->page_content [ 'a024' ] > 1 )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Heupwijdte : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a024' ] , "s{ KOM1 }" ) . " cm" ,   "", "" ) ) );	
	if ( $this->page_content [ 'a026' ] > 1 )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Handknijpkracht links : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a026' ] , "s{ KOMMA }" ) . " kg" ,   "", "" ) ) );	
	if ( $this->page_content [ 'a027' ] > 1 )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Handknijpkracht rechts : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a027' ] , "s{ KOMMA }" ) . " kg" ,   "", "" ) ) );	
	$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Activiteit level: ", "", $this->language [ 'beweging' ] [ $this->page_content [ 'a025' ] ],   "", "" ) ) );
	$pdf->DataKolom ( $ral, $pdf_cols );
	$ral = array ();
//	$pdf_text .= '* '. $this->language [ 'antropo' ] [ 'b025' ]. "\n";
	$pdf->ChapterKlein( $pdf_text );
	$pdf_text = "";
}

if ( $pdf ) {
	if ( $this->page_content [ 'a008' ] ) {
		$pdf_text .= "\n".'Gewichtsanalyse'. "\n";
		$pdf->ChapterLarge( $pdf_text );
		$pdf_text = "";
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "BMI (Body Mass Index) : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a031' ] , "s{ KOM1 }" ),  $this->page_content [ 'a032' ], "" ) ) );	
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Bij de lengte past een gewicht van :  ", "",  $this->gsm_sanitizeStrings (  $this->page_content [ 'a033' ] , "s{ WHOLE }" ) . " - " . $this->gsm_sanitizeStrings (  $this->page_content [ 'a034' ] , "s{ WHOLE }" ).' kg',  "", "" ) ) );	
	if ( $this->page_content [ 'a035' ] > 1 ) {
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Er zou dus bij mogen :  ", "",  $this->gsm_sanitizeStrings (  $this->page_content [ 'a035' ] , "s{ WHOLE }" ).' kg',  "", "" ) ) );	
	}
	if ( $this->page_content [ 'a035' ] < - 1 ) {
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Er zou dus af mogen :  ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a035' ] * -1 , "s{ WHOLE }" ).' kg',  "", "" ) ) );	
	}
		$pdf->DataKolom ( $ral, $pdf_cols );
		$ral = array ();
	}	
}

if ( $pdf ) {
	if ( $this->page_content [ 'a007' ] ) {
		$pdf_text .= "\n".'Buikomvang analyse'. "\n";
		$pdf->ChapterLarge( $pdf_text );
		$pdf_text = "";
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Buikomvang : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a022' ] , "s{ KOM1 }" )  . " cm", $this->page_content [ 'a038' ], "" ) ) );
		if 	( $this->page_content [ 'a047' ] != "" )		
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Bij u past een buikomvang van : ", "",  $this->page_content [ 'a047' ] . " cm",  "", "" ) ) );
		$pdf->DataKolom ( $ral, $pdf_cols );
		$ral = array ();
	}	
}

if ( $pdf ) {
	if ( $this->page_content [ 'a006' ] ) {
		$pdf_text .= "\n".'Lichaamsvet analyse'. "\n";
		$pdf->ChapterLarge( $pdf_text );
		$pdf_text = "";

		if ( $this->page_content [ 'a041' ] > $this->page_content [ 'a040' ] ) {
			$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Vet percentage : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a040' ] , "s{ WHOLE }" ) . " - " . $this->gsm_sanitizeStrings (  $this->page_content [ 'a041' ] , "s{ WHOLE }" ). " %", $this->page_content [ 'a042' ], "" ) ) );
		} else {
			$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Vet percentage : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a041' ] , "s{ WHOLE }" ) . " - " . $this->gsm_sanitizeStrings (  $this->page_content [ 'a040' ] , "s{ WHOLE }" ). " %", $this->page_content [ 'a042' ], "" ) ) );
		}
	} else {
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Vet percentage : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a040' ] , "s{ WHOLE }" ) . " %", $this->page_content [ 'a042' ], "" ) ) );
	}
	if 	( $this->page_content [ 'a043' ] != "" )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Bij u past een vet percentage van : ", "", $this->page_content [ 'a043' ] . " %", "", "" ) ) );

	if 	($this->page_content [ 'a048' ] != 1 )
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "MHR (Middel-Heup Ratio) :  ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a048' ] , "s{ KOMMA }" ) , $this->page_content [ 'a042' ], "" ) ) );

	$pdf->DataKolom ( $ral, $pdf_cols );
	$ral = array ();
}

if ( $pdf ) {
	if ( $this->page_content [ 'a026' ] > 1 && $this->page_content [ 'a027' ] > 1 ) {
		$pdf_text .= "\n".'Spierkracht analyse'. "\n";
		$pdf->ChapterLarge( $pdf_text );
		$pdf_text = "";

		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Dominantie : ", "", $this->page_content [ 'a060' ] , "" , "" ) ) );
		$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Handknijpkracht : ", "", $this->gsm_sanitizeStrings ( $this->page_content [ 'a061' ]  , "s{ KOMMA }" ) . ' kg' , $this->page_content [ 'a063' ], "" ) ) );
		if ( $this->page_content [ 'a061' ] < $this->page_content [ 'a062' ])
			$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Bij u past een handknijpkracht van minstens : ", "",  $this->gsm_sanitizeStrings ( $this->page_content [ 'a062' ]  , "s{ WHOLE }" ) . ' kg' , "", "" ) ) );
		$pdf->DataKolom ( $ral, $pdf_cols );
		$ral = array ();
	}
}

if ( $pdf  && $this->page_content [ 'a008' ] ) {
	$pdf_text .= "\n".'Calorie verbruik berekening'. "\n";
	$pdf->ChapterLarge( $pdf_text );
	$pdf_text = "";
	$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "BMR (Calorie verbruik in complete rust) :  ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a050' ] , "s{ WHOLE }" ) . " kcal", "", "" ) ) );
	$ral [ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s", " ", "Calorie behoefte bij activiteiten level : ", "", $this->gsm_sanitizeStrings (  $this->page_content [ 'a051' ] , "s{ WHOLE }" ) . " kcal", "", "" ) ) );
	$pdf->DataKolom ( $ral, $pdf_cols );
	
	$ral = array ();
}

	/* uitleg van de berekeningen */
if ($pdf && $this->setting [ 'debug' ] == "yes" ) {
	$pdf_text .= "\n\n";
	$pdf_text .= "\n\n" ."De gebruikte berekenings methodes en grenswaardes.";
	$pdf_text .= "\n\n" ."Berekenings methode leeftijd : Een standaard berekenings methode op basis van geboortedatum en datum tijd stempel. Als de leeftijd beneden de 20 is wordt --buiten (tabel) bereik-- genoteerd. Voor kinderen zijn de gebrukte conclusies op basis van de tabellen te vaak niet passend.";
	$pdf_text .= "\n\n" ."Berekenings methode BMI : De standaard berekenings methode op basis van kwadraat van de lengte in meters en het gewicht in kg waarbij de BMI de ratio is";
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes BMI : tot 70 jaar";
	foreach ( $this->language [ 'BMI' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot BMI %s : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes BMI : vanaf 70 jaar";
	foreach ( $this->language [ 'BMI70' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot BMI %s : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Berekenings methode gewichts range : Op basis van de als normaal aangegeven BMI boven en ondergrens wordt het gewicht terug gerekend";
	$pdf_text .= "\n\n" ."Signalering wat er bij of eraf zou moeten: het actuele gewicht wordt geplaatst in de gewichts range waaruit de target minimale afname of toename wordt bepaald (indien het gewicht niet in de range is) ";
	$pdf_text .= "\n\n" ."De gemeten buikomvang wordt gelegd tegen de gangbare tabellen voor buikomvang. Er is een andere table voor mannen en vrouwen.";
	$pdf_text .= "\n\n" ."Buikomvang analyse";	
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes buikomvang vrouwen";
	foreach ( $this->language [ 'buikvrouw' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s cm : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes buikomvang mannen";
	foreach ( $this->language [ 'buikman' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s cm : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."De range verhoogd risico wordt aangenomen als de grenswaardes voor buikomvang.";
	$pdf_text .= "\n\n" ."Buikvet analijse: ";
	$pdf_text .= "\n\n" ."Er is een algoritme gebaseerd op de BMI, leeftijd en man/vrouw. Deze wordt toegepast";
	$pdf_text .= "\n\n" ."Er ook een algoritme rekening houdend met de lichaamsvorm ( appel of peer vorm ) berekend via de lengte, nekwijdte, heupwijdte en buikomvang en Man/vrouwg Deze wordt ook toegepast als de data aangegeven is. ";
	$pdf_text .= "\n\n" ."De resultaten van de twee berekeningen wordt gepresenteerd als als een range.";
	$pdf_text .= "\n\n" ."Deze range wordt met de norm vergeleken voor een qualificatie.";
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes buikvet vrouwen 20- 40 jaar";
	foreach ( $this->language [ '20vrouw' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s perc : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes buikvet mannen 20-40 jaar";	
	foreach ( $this->language [ '20man' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s perc : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes buikvet vrouwen 40-60 jaar";
	foreach ( $this->language [ '40vrouw' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s perc : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes buikvet mannen 40-60 jaar";	
	foreach ( $this->language [ '40man' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s perc : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes buikvet vrouwen > 60 jaar";
	foreach ( $this->language [ '60vrouw' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s perc : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes buikvet mannen > 60 jaar";	
	foreach ( $this->language [ '60man' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s perc : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Buikholte vet heeft een ander effect op de oe gezondheid als vet op de heupen. De MHR (middel-heup ratio) wordt berekend en gerapporteerd bij > als de grenswaarde ( MHR >95% voor mannen en MHR >80 voor vrouwen ).";
	$pdf_text .= "\n\n" ."Knijpkracht analyse: ";
	$pdf_text .= "\n\n" ."De dominantie wordt bepaald ";
	$pdf_text .= "\n\n" ."Alleen de met de hoogste waarde wordt gewerkt en vergeleken met de normtabel 10percentiel. Deze is gedifferentieerd naar leeftijd en man / vrouw";
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes knijpkracht vrouwen ";	
	foreach ( $this->language [ 'knijpvrouw' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s jaar : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Toegepaste grenswaardes knijpkracht mannen ";	
	foreach ( $this->language [ 'knijpman' ] as $key => $value ) $pdf_text .= "\n" .sprintf ( "- tot %s jaar : %s", $key, $value ) ;
	$pdf_text .= "\n\n" ."Als de BMI Normaal is worden een aantal qualificaties onderdrukt volgens geaccepteerde prioriteiten.";
	$pdf_text .= "\n\n" ."Calorie berekeningen: ";
	$pdf_text .= "\n\n" ."Op basis van een algoritmes en de ingevoerde gegevens wordt bepaald het calorie verbruik als men helemaal niets doet (BMR). n.b. zelfs niet erover nadenkt !";
	$pdf_text .= "\n\n" ."Afhankelijk van het aangegeven activiteiten niveau wordt een schatting gemaakt van het aantal calorieen dat men dagelijks nodig heeft. ";	
	$pdf_text .= "\n\n\n\n\n\n";
	$pdf->ChapterBody( $pdf_text );
	$pdf_text = "";
}

// pdf afsluiting

if ($pdf) {
	$pdf_text .= "\n\n\n\n\n\n"."Dit antropometrie overzicht en de berekeningen, gebaseerd op de ingevoerde gegevens, is "; 
	if ( LOAD_OLINE ) {
		$pdf_text .= sprintf ( "on-line via %s" , LEPTON_URL );
	} else {
		$pdf_text .= "off-line";
	}
	$pdf_text .= " gemaakt op : " . $this->gsm_sanitizeStrings ( date ( "d M Y", time ( ) ), "s{ DATUM }" ) ; 
//	$pdf_text .= ". Antropometrie is het vaststellen van afmetingen en verhoudingen van het menselijk lichaam waardoor een uitspraak kan worden gedaan over de gezondheid.". "\n"; 
	$pdf_text .= " De berekeningen en uitspraken, gedaan op basis van de ingevoerde gegevens en verwerkt volgens de gangbare normen en algoritmes, zijn met zorgvuldigheid gedaan. ";
	$pdf_text .= " De vereniging Achtse Belangen aanvaardt geen enkele aansprakelijkheid naar aanleiding van deze berekening en de uitspraken. " ; 
	$pdf_text .= " Heeft u vragen of opmerkingen, neem dan gerust contact op :" . "\n\n"; 
	$pdf_text .= " De vereniging Achtse Belangen " . "\n" . "https://ab.acht.nl"; 
	$pdf->ChapterFooter( $pdf_text );	
}
?>