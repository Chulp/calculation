<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

//check values

/* debug * / Gsm_debug ( array ( $oFC->page_content ), __LINE__ . __FUNCTION__ ); /* debug */ 
$oFC->page_content [ 'a020' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'a020' ] ?? "0", "v{". $oFC->page_content [ 'z020' ].";20;220}" );	
$oFC->page_content [ 'a021' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'a021' ] ?? "0", "v{". $oFC->page_content [ 'z021' ].";20;220}" );	
$oFC->page_content [ 'a022' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'a022' ] ?? "", "v{". "".";20;220}" );	
$oFC->page_content [ 'a023' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'a023' ] ?? "", "v{". "".";20;220}" );	
$oFC->page_content [ 'a024' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'a024' ] ?? "", "v{". "".";20;220}" );	
$oFC->page_content [ 'a026' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'a026' ] ?? "0", "v{". "0;0;200}" );	
$oFC->page_content [ 'a027' ] = $oFC->gsm_sanitizeStringV ( $oFC->page_content [ 'a027' ] ?? "0", "v{". "0;0;200}" );	
$oFC->page_content [ 'a001' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'a001' ] ?? "--", "s{ TOASC|CLEAN|TRIM }" );
$oFC->page_content [ 'a002' ] = $oFC->gsm_sanitizeStringD ( $oFC->page_content [ 'a002' ] ?? $oFC->language ['antropo'][ 'z001' ], "y{". $oFC->language ['antropo'][ 'z001' ]. ";" . $oFC->language ['antropo'][ 'z002' ] . ";" . $oFC->page_content [ 'DATE' ] ."}" );
$oFC->page_content [ 'a009' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content [ 'a009' ] ?? "", "s{ TOASC|CLEAN|TRIM } " );
?>