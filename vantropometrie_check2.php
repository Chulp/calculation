<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
$oFC->page_content [ 'a008' ] = true ;  // leeftijd buiten range 
$oFC->page_content [ 'a007' ] = false ;  // Buikomvang beschikbaar
if ( $_POST [ 'gsm_a022' ] > 1 ) $oFC->page_content [ 'a007' ] = true ; 
$oFC->page_content [ 'a006' ] = false ;  // Vormfactor beschikbaar
$oFC->page_content [ 'a005' ] = true ;  // toon alle defects
/* debug * / Gsm_debug (array ($oFC->page_content, $oFC->language, $_POST), __LINE__ . __FUNCTION__ ); /* debug */
$oFC->page_content [ 'RAPPORTAGE' ] .= '<table>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6"><h3>Antropometrie Meting / Overzicht</h3></td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">Datum vaststelling : ' . $oFC->gsm_sanitizeStrings ( date ( "d M Y", time ( ) ), "s{ DATUM }" ) . '</td><td colspan="4"></td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Gegevens persoon</h4></td><td colspan="4"></td></tr>';
/* Naam */
if ( strlen ( $oFC->page_content [ 'a001' ] ) > 3 ) $oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2">Naam : </td><td></td><td colspan="2"><strong>' . $oFC->page_content [ 'a001' ] . '</strong></td><td></td></tr>';	
/* Adres */
$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Geslacht : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->page_content [ 'a009' ] . '</td><td></td></tr>';	
/* Geboortedatum */
if ( $oFC->page_content [ 'a002' ] != $oFC->language [ 'antropo' ][ 'z002' ] )	
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Geboorte datum : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'a002' ], "s{ DATUM }" ) . '</td><td></td></tr>';	
/* maten */
if ( $oFC->page_content [ 'a020' ] > 20 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Gewicht : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a020' ] , "s{ KOM1 }" ) . " kg". '</td><td></td></tr>';	
if ( $oFC->page_content [ 'a021' ] > 100 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Lengte : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a021' ] , "s{ KOM1 }" ) . " cm". '</td><td></td></tr>';	
if ( $oFC->page_content [ 'a023' ] > 10 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Nek Wijdte : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a023' ] , "s{ KOM1 }" ) . " cm". '</td><td></td></tr>';	
if ( $oFC->page_content [ 'a022' ] > 20 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Buikomvang : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a022' ] , "s{ KOM1 }" ) . " cm". '</td><td></td></tr>';	
if ( $oFC->page_content [ 'a024' ] > 20 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Heup wijdte : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a024' ] , "s{ KOM1 }" ) . " cm". '</td><td></td></tr>';	
if ( $oFC->page_content [ 'a026' ] > 1 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Handknijpkracht LINKS : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a026' ] , "s{ KOM1 }" ) . " kg". '</td><td></td></tr>';	
if ( $oFC->page_content [ 'a027' ] > 1 )
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Handknijpkracht RECHTS : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a027' ] , "s{ KOM1 }" ) . " kg". '</td><td></td></tr>';	
$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Activiteit level : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->language [ 'beweging' ] [ $oFC->page_content [ 'a025' ] ] . '</td><td></td></tr>';	
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"></td><td colspan="4"></td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Berekeningen</h4></td><td colspan="4"></td></tr>';

/* 30 berekening leeftijd */
$Lhulp01 = $oFC->page_content [ 'a002' ]; // birth
$Lhulp02 = date ( "Y-m-d", time ( ) ); // nu
$oFC->page_content [ 'a030' ] = substr ( $Lhulp02,0,4) - substr ( $Lhulp01,0,4);
if ( substr ( $Lhulp01,5,5) > substr ( $Lhulp02,5,5) ) $oFC->page_content [ 'a030' ]--;  //leeftijd
if ( $oFC->page_content [ 'a002' ] == $oFC->language [ 'antropo' ][ 'z002' ] || $oFC->page_content [ 'a030' ] < 20 )	{
	$oFC->page_content [ 'a008' ] = false ;
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Leeftijd : </td><td></td><td style="text-align:right;" colspan="2">Buiten reken grenzen</td><td></td></tr>';	
} else {	
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Leeftijd : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->page_content [ 'a030' ] . '</td><td></td></tr>';	
} 

/* 31 berekening BMI */
$Lhulp01 =  $oFC->page_content [ 'a021' ] / 100 ;
$oFC->page_content [ 'a031' ] = round ( $oFC->page_content [ 'a020' ] / $Lhulp01  / $Lhulp01, 1 );

/* 32 qualificatie bij BMI */
$oFC->page_content [ 'a032' ] = "";
$switch = true;
foreach ( $oFC->language [ 'BMI' ] as $key => $value ) {
	if ( $switch ) {
		$oFC->page_content [ 'a032' ] = $value;
		if ( $key >= $oFC->page_content [ 'a031' ] ) $switch = false;
	}
}
$oFC->page_content [ 'a033' ] = 18.5; 
$oFC->page_content [ 'a034' ] = 25;

/* 32 qualificatie bij BMI > 69 leeftijd */
if ( $oFC->page_content [ 'a030' ] > 69 ) {
	$oFC->page_content [ 'a032' ] = "";
	$switch = true;
	foreach ( $oFC->language [ 'BMI70' ] as $key => $value ) {
		if ( $switch ) {
			$oFC->page_content [ 'a032' ] = $value;
			if ( $key >= $oFC->page_content [ 'a031' ] ) $switch = false;
		}
	}
	$oFC->page_content [ 'a033' ] = 22; 
	$oFC->page_content [ 'a034' ] = 28;
}

/* 33 gewicht bij BMI ondergrensgrens */
$oFC->page_content [ 'a033' ] = round ( $oFC->page_content [ 'a033' ] * $Lhulp01  * $Lhulp01, 0 );

/* 34 gewicht bij BMI bovengrens */
$oFC->page_content [ 'a034' ] = round ( $oFC->page_content [ 'a034' ] * $Lhulp01  * $Lhulp01, 0 );

/* 35 wat moet eraf erbij */
if  ( $oFC->page_content [ 'a020' ] > $oFC->page_content [ 'a034' ] ) { // eraf
		$oFC->page_content [ 'a035' ] = floor ($oFC->page_content [ 'a034' ] - $oFC->page_content [ 'a020' ]);
} elseif  ( $oFC->page_content [ 'a020' ] < $oFC->page_content [ 'a033' ] ) { // erbij
	$oFC->page_content [ 'a035' ] = floor ($oFC->page_content [ 'a020' ] - $oFC->page_content [ 'a033' ]);
} else {
	$oFC->page_content [ 'a035' ] = 0;
}
/* suppress fault message on other places */
if ( strpos ( "normaal", strtolower ($oFC->page_content [ 'a032' ] ) ) !== false) $oFC->page_content [ 'a005' ] = false;

/* display */
if ( $oFC->page_content [ 'a008' ] ) {
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"></td><td colspan="4"></td></tr>';
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Gewichts analyse</h4></td><td colspan="4"></td></tr>';
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">BMI (Body Mass Index) : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a031' ] , "s{ KOM1 }" ) . '</td><td>' . $oFC->page_content [ 'a032' ] . '</td></tr>';	
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Bij de lengte past een gewicht van : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a033' ] , "s{ WHOLE }" ) . " - " . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a034' ] , "s{ WHOLE }" ).' kg</td><td></td></tr>';	
	if ( $oFC->page_content [ 'a035' ] > 1 ) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Er zou dus bij mogen : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a035' ] , "s{ WHOLE|STRONG }" ).'<strong> kg</strong></td><td></td></tr>';	
	}
	if ( $oFC->page_content [ 'a035' ] < - 1 ) {
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Er zou dus af mogen : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a035' ] * -1 , "s{ WHOLE|STRONG }" ).'<strong> kg</strong></td><td></td></tr>';	
	}
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"></td><td colspan="4"></td></tr>';
} 

/* buikomvang */
if ( $oFC->page_content [ 'a007' ] ) {
/* 39 qualificatie bij buikomvang */
	$oFC->page_content [ 'a039' ] = "";
	$switch = true;
	if ( $oFC->page_content [ 'a009' ] == "man") {
		foreach ( $oFC->language [ 'buikman' ] as $key => $value ) {
			if ( $switch ) {
				$oFC->page_content [ 'a038' ] = $value;
				if ( $key >= $oFC->page_content [ 'a022' ] ) $switch=false;
			}
		}
	} else {
		foreach ( $oFC->language [ 'buikvrouw' ] as $key => $value ) {
			if ( $switch ) {
				$oFC->page_content [ 'a038' ] = $value;
				if ( $key >= $oFC->page_content [ 'a022' ] ) $switch=false;
			}
		}
	}
/* 47 advies buikomvang */
	$oFC->page_content [ 'a047' ] = "";
	if ( $oFC->page_content [ 'a009' ] == "man" ) {
		$Lhulp01 = 100;
		if ( $oFC->page_content [ 'a038' ] != $oFC->language [ 'buikman' ] [ $Lhulp01 ] ) 	$oFC->page_content [ 'a047' ] = "94 - 100";
	} else { 
		$Lhulp01 = 90; 
		if ( $oFC->page_content [ 'a038' ] != $oFC->language [ 'buikvrouw' ] [ $Lhulp01 ] ) 	$oFC->page_content [ 'a047' ] = "80 - 90";
	}
	if ( !$oFC->page_content [ 'a005' ] )  $oFC->page_content [ 'a038' ] = "";
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Buikomvang analyse</h4></td><td colspan="4"></td></tr>';
	$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Buikomvang : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a022' ] , "s{ KOM1 }" ) . ' cm</td><td>' . $oFC->page_content [ 'a038' ] . '</td></tr>';	
	if 	($oFC->page_content [ 'a047' ] != "" )
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Bij u past een buikomvang van : </td><td></td><td style="text-align:right;" colspan="2"><strong>' .  $oFC->page_content [ 'a047' ] . ' cm</strong></td><td></td></tr>';	
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"></td><td colspan="4"></td></tr>';
}

/* Lichaamsvet */
if ( $oFC->page_content [ 'a008' ] ) {

/* BMI only method */	
/* 40 qualificatie vet percentage BMI methode */
	$Lhulp01 = $oFC->page_content [ 'a031' ]; // BMI
	$Lhulp02 = $oFC->page_content [ 'a030' ]; // leeftijd
	if ( $oFC->page_content [ 'a009' ] == "man") {
		$Lhulp03 = $oFC->page_content [ 'a031' ] * 1.2; 
		$Lhulp04 = $oFC->page_content [ 'a030' ] * 0.23; 
		$oFC->page_content [ 'a040' ] = round ($Lhulp03 + $Lhulp04 - 16.2, 1);
	} else {
		$Lhulp03 = $oFC->page_content [ 'a031' ] * 1.2; 
		$Lhulp04 = $oFC->page_content [ 'a030' ] * 0.23; 
		$oFC->page_content [ 'a040' ] = round ($Lhulp03 + $Lhulp04 - 5.4, 1);
	}
	
/* Measurement method */	
/* 41 brekening vet percentage */	
/* 48 BHR buikholte vet */
	$Lhulp01 = $oFC->page_content [ 'a021' ] ; // lengte
	$Lhulp02 = $oFC->page_content [ 'a022' ] ; // buik
	$Lhulp03 = $oFC->page_content [ 'a023' ] ; // boord
	$Lhulp04 = $oFC->page_content [ 'a024' ] ; // heup

	if ( $oFC->page_content [ 'a009' ] == "man") {
		$Lhulp05 = $Lhulp01 * $Lhulp02 * $Lhulp03;
		if ( $Lhulp05 > 0 ) $oFC->page_content [ 'a006' ]= true;
	} else {
		$Lhulp05 = $Lhulp01 * $Lhulp02 * $Lhulp03 * $Lhulp03;
		if ( $Lhulp05 > 0 ) $oFC->page_content [ 'a006' ]= true;
	}
	$oFC->page_content [ 'a048' ] = 1;
	if ( $oFC->page_content [ 'a006' ] ) {
		if ( $oFC->page_content [ 'a009' ] == "man") {
			$Lhulp06 = $Lhulp02 - $Lhulp03;
			$Lhulp06 = log ($Lhulp06, 10);
			$Lhulp06 = 0.19077 * $Lhulp06;
			$Lhulp07 = log ($Lhulp01, 10);
			$Lhulp07 = 0.15456 * $Lhulp07;
			$Lhulp08 = 1.0324 - $Lhulp06  + $Lhulp07;
			$oFC->page_content [ 'a041' ] = round (495 / $Lhulp08 - 450, 1);
			if ($oFC->page_content [ 'a024' ] > 20) $oFC->page_content [ 'a048' ] = round($oFC->page_content [ 'a022' ] / $oFC->page_content [ 'a024' ],2);
			
		} else {
			$Lhulp06 = $Lhulp02 + $Lhulp04 - $Lhulp03;
			$Lhulp06 = log ($Lhulp06, 10);
			$Lhulp06 = 0.35004 * $Lhulp06;
			$Lhulp07 = log ($Lhulp01, 10);
			$Lhulp07 = 0.221 * $Lhulp07;
			$Lhulp08 = 1.29579 - $Lhulp06  + $Lhulp07;
			$oFC->page_content [ 'a041' ] = round (495 / $Lhulp08 - 450, 1);
			if ($oFC->page_content [ 'a024' ] > 20) $oFC->page_content [ 'a048' ] = round ($oFC->page_content [ 'a022' ] / $oFC->page_content [ 'a024' ],2);
		}
	} else {
		$oFC->page_content [ 'a041' ] = 0; // geen berekening op basis van vormfactor
	}

/* 42 qualificatie vet percentage */	
	$oFC->page_content [ 'a042' ] = "";
	$switch = true;
	if ( $oFC->page_content [ 'a030' ] > 60 ) {		
		if ( $oFC->page_content [ 'a009' ] == "man") {
			foreach ( $oFC->language [ '60man' ] as $key => $value ) {
				if ( $switch ) {
					$oFC->page_content [ 'a042' ] = $value;
					if ( $key >= $oFC->page_content [ 'a040' ] ) $switch=false;
				}
			}
		} else {
			foreach ( $oFC->language [ '60vrouw' ] as $key => $value ) {
				if ( $switch ) {
					$oFC->page_content [ 'a42' ] = $value;
					if ( $key >= $oFC->page_content [ 'a040' ] ) $switch=false;
	}	}	}	}
/* 49 buikholte vet */	
	$oFC->page_content [ 'a049' ] = "";
	if ( $oFC->page_content [ 'a048' ] != 1 ) {		
		if ( $oFC->page_content [ 'a009' ] == "man") {
			if ( $oFC->page_content [ 'a048' ] > 0.95 ) $oFC->page_content [ 'a049' ] = $oFC->language [ "MHR"];
		} else {
			if ( $oFC->page_content [ 'a048' ] > 0.80 ) $oFC->page_content [ 'a049' ] = $oFC->language [ "MHR"];
	}	}	
	
/* 43 advies vetpercentage */
	if ( $oFC->page_content [ 'a009' ] == "man") { 
		if ($oFC->page_content [ 'a030' ] > 19) { $Lhulp01 = 20; $Lhulp02 = 19.9; }
		if ($oFC->page_content [ 'a030' ] > 39) { $Lhulp01 = 40; $Lhulp02 = 22.9; }
		if ($oFC->page_content [ 'a030' ] > 59) { $Lhulp01 = 60; $Lhulp02 = 24.9; }
		$Lhulp01 = $Lhulp01 . 'man';
	} else {
		if ($oFC->page_content [ 'a030' ] > 19) { $Lhulp01 = 20; $Lhulp02 = 39; }
		if ($oFC->page_content [ 'a030' ] > 39) { $Lhulp01 = 40; $Lhulp02 = 40; }	
		if ($oFC->page_content [ 'a030' ] > 59) { $Lhulp01 = 60; $Lhulp02 = 42; }	
		$Lhulp01 = $Lhulp01 . 'vrouw';
	}
	$Lhulp03 = "";
	$Lhulp04 = "";
	$oFC->page_content [ 'a043' ] = "";
	foreach ( $oFC->language [ $Lhulp01 ] as $key => $value) {
		if ( $Lhulp04 == "" ) {
			if ( $Lhulp02 > $key ) {
				$Lhulp03 = $key;
			} else {
				$oFC->page_content [ 'a043' ] = $Lhulp03 . " - " . $key;
				$Lhulp04 = $value;
	}	}	}
	if ( !$oFC->page_content [ 'a005' ] )  $oFC->page_content [ 'a042' ] = "";
	if ( !$oFC->page_content [ 'a005' ] )  $oFC->page_content [ 'a049' ] = "";
	if ($Lhulp04 == $oFC->page_content [ 'a042' ] ) $oFC->page_content [ 'a043' ] = '';
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Lichaamsvet analyse</h4></td><td colspan="4"></td></tr>';	
	if ( $oFC->page_content [ 'a006' ] ) {			
		if ( $oFC->page_content [ 'a041' ] > $oFC->page_content [ 'a040' ] ) {
			$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Vet percentage : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a040' ] , "s{ WHOLE }" ) . " - " . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a041' ] , "s{ WHOLE }" ).  ' %</td><td>' . $oFC->page_content [ 'a042' ] . '</td></tr>';	
		} else {
			$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Vet percentage : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a041' ] , "s{ WHOLE }" ) . " - " . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a040' ] , "s{ WHOLE }" ).  ' %</td><td>' . $oFC->page_content [ 'a042' ] . '</td></tr>';	
		}
	} else {
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Vet percentage : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a040' ] , "s{ KOM1 }" ) . ' %</td><td>' . $oFC->page_content [ 'a042' ] . '</td></tr>';
	}
	if 	($oFC->page_content [ 'a043' ] != "" )
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Bij u past een vet percentage van : </td><td></td><td style="text-align:right;" colspan="2"><strong>' .  $oFC->page_content [ 'a043' ] . ' %</strong></td><td></td></tr>';	
	if 	($oFC->page_content [ 'a048' ] != 1 )
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">MHR (Middel-Heup Ratio) : </td><td></td><td style="text-align:right;" colspan="2">' .  $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a048' ] , "s{ KOMMA }" ) . '</td><td>' . $oFC->page_content [ 'a049' ] . '</td></tr>';
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"></td><td colspan="4"></td></tr>';
/* 60 dominantie */ 
/* 61 waarde */
	if ( $oFC->page_content [ 'a026' ] > 1 && $oFC->page_content [ 'a027' ] > 1 ) {
		if ( $oFC->page_content [ 'a026' ] > $oFC->page_content [ 'a027' ] ) {
			$oFC->page_content [ 'a060' ] = "LINKS";
			$oFC->page_content [ 'a061' ] = $oFC->page_content [ 'a026' ];
			
		} else {
			$oFC->page_content [ 'a060' ] = "RECHTS";
			$oFC->page_content [ 'a061' ] = $oFC->page_content [ 'a027' ];
			
		}
		
/* 62 Qualificatie */	
/* leeftijd naar tabewaarde conversie */		
	$oFC->page_content [ 'a065' ] = round ( $oFC->page_content [ 'a030' ] / 5, 0 ) * 5;
	if ( $oFC->page_content [ 'a009' ] == "man") {
		$oFC->page_content [ 'a062' ] = $oFC->language [ 'knijpman' ] [ $oFC->page_content [ 'a065' ] ];
	} else {
		$oFC->page_content [ 'a062' ] = $oFC->language [ 'knijpvrouw' ] [ $oFC->page_content [ 'a065' ] ];
	}
	$oFC->page_content [ 'a063' ] = "";
	if ( $oFC->page_content [ 'a061' ] < $oFC->page_content [ 'a062' ]) {
		$oFC->page_content [ 'a063' ] = "Te laag";
	}

/* debug * /  echo(LEPTON_tools::display(array( "regel" => __LINE__, $oFC->page_content [ 'a030' ], $oFC->page_content [ 'a060' ], $oFC->page_content [ 'a061' ], $oFC->page_content [ 'a062' ], $oFC->page_content [ 'a063' ], $oFC->page_content [ 'a065' ]),'pre','ui message')); /* debug */

		$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Spierkracht analyse</h4></td><td colspan="4"></td></tr>';	
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Dominantie : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->page_content [ 'a060' ] . '</td><td></td></tr>';	
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Handknijpkracht : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'a061' ]  , "s{ KOMMA }" ) . ' kg</td><td>' . $oFC->page_content [ 'a063' ] . '</td></tr>';	
		if ( $oFC->page_content [ 'a061' ] < $oFC->page_content [ 'a062' ])
			$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Bij u past een handknijpkracht van minstens : </td><td></td><td style="text-align:right;" colspan="2"><strong>' . $oFC->gsm_sanitizeStrings ( $oFC->page_content [ 'a062' ]  , "s{ WHOLE }" ) . ' kg</strong></td><td></td></tr>';	
		$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"></td><td colspan="4"></td></tr>';
	}
	
	/* 50 BMR  */
	/* 51 calorie verbruik  */
	$Lhulp01 = $oFC->page_content [ 'a020' ] ; // gewicht
	$Lhulp02 = $oFC->page_content [ 'a021' ] ; // lengte
	$Lhulp03 = $oFC->page_content [ 'a030' ] ; // leeftijd
	$Lhulp04 = $oFC->page_content [ 'a041' ] ; // vet %
	$Lhulp05 = $oFC->language [ 'beweging2' ] [ $oFC->page_content [ 'a025'] ] ; // activiteit level
	$Lhulp06 = ( 10 * $oFC->page_content [ 'a020' ] ) + ( 6.25 * $oFC->page_content [ 'a021' ]) - ( 5 *  $oFC->page_content [ 'a030' ] );
	
	/* 51 calorie verbruik  */
	
	if ( $oFC->page_content [ 'a009' ] == "man") {
		$oFC->page_content [ 'a050' ] = round ( $Lhulp06 , 0) + 5;
		$oFC->page_content [ 'a051' ] = round ( $oFC->page_content [ 'a050' ] * $Lhulp05 , 0);
	} else {
		$oFC->page_content [ 'a050' ] = round ( $Lhulp06 , 0) - 161;
		$oFC->page_content [ 'a051' ] = round ( $oFC->page_content [ 'a050' ] * $Lhulp05 , 0);
	}
	
	if ( $oFC->page_content [ 'a008' ] ) {	
		$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"></td><td colspan="4"></td></tr>';
		$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="2"><h4>Calorie gebruik berekeningen</h4></td><td colspan="4"></td></tr>';
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">BMR (Calorie verbruik in complete rust) : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a050' ] , "s{ WHOLE }" ) . ' kcal</td><td></td></tr>';	
		$oFC->page_content [ 'RAPPORTAGE' ] .= '</td><td colspan="2">Calorie behoefte bij activiteiten level : </td><td></td><td style="text-align:right;" colspan="2">' . $oFC->gsm_sanitizeStrings (  $oFC->page_content [ 'a051' ] , "s{ WHOLE }" ) . ' kcal</td><td></td></tr>';
	}
	$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';
}

$oFC->page_content [ 'RAPPORTAGE' ] .= '<tr><td colspan="6">  </td></tr>';
$oFC->page_content [ 'RAPPORTAGE' ] .= '</table>';
?>