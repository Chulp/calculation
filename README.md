# gsmoffc  / calculation

 Lepton base for calculation functions as part of the gsmoff set of applications.

This document is relevant for `gsmoffc` / `Calculation`  version 1.1.0 and up.

## Download
The released stable `gsmoffc` / `Calculation` application is recommended to install/update to the latest available version listed. Older versions may contain bugs, lack newer functions or have security issues. 

## License
`gsmoffc` / `Calculation` is licensed under the [GNU General Public License (GPL) v3.0](http://www.gnu.org/licenses/gpl-3.0.html). The module is available "as is"

### You are free to:
Share — copy and redistribute the material in any medium or format
Adapt — remix, transform, and build upon the material
for any purpose, even commercially. 

### Under the following terms:
Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

## Pre-conditions
The minimum requirements to get this application running on your LEPTON installation are as follows:

- LEPTON *** 7.0 *** of higher depending on the version
- the module uses Twig support
- the module uses Fomantic
- the taxonomy module installed.

The module is tested in combination with the Office Tegel template

## Installation

This description assumes you have a standard installation at the at least at level Lepton 7.0. All tests are done exclusively with the TFA during installation selected off (not ticked) *)  

1. download the installation package
2. install the downloaded zip archive via the LEPTON module installer
3. create a page which uses the indicated module
4. start the backend functions setupc/instellingen and optionally activate one of the following functions where needed 

  * IMAGE 	image directory copied from frontend
  * LOGGING 	empty the logging
  * DETAIL 	detailed data
  * INSTALL 	frontend files are created: customized values are overwritten bij default values !!
or enter ? also for not (yet) documented functions

The function can be started by selecting d_...._ where ... is the function name. A number of the function names can be concatenated.

The system will automatically install the file upon the first use of this module. 
 
### the backend functions

- setupa / instellingen	to setup and maintenace of the data

 Depending on the contents of the SET file modules are made available it may be needed to modify it to get the required functionality
  
 ### frontend menu
 
- antropometrie Antropometrie Analyse based on length, weight and width of a person an analyse
- transitie     Calculation: "Berekening Transitie Vergoeding"  in accordance with the dutch regulations ex artikel 7:673 BW
- calculation   This s an example module as a base for new calculations

As both functions are quite different in application area it isprobably needed to switch of the functionality of one of them : the adapt the SET.php file with the templates frontend  

### frontend menu  SET_menu

$FC_SET [ 'SET_function' ] 	= 'setupc/dummy'; 	// backend menu ';
if ( $section_id == 17 ) { 
	$FC_SET [ 'SET_menu' ] 		= 'calculation';  // frontend menu ';
} else { 
	$FC_SET [ 'SET_menu' ] 		= 'antropometrie';  // frontend menu ';
}

// for the administrator and the editor
if ( isset ($_SESSION [ 'GROUPS_ID' ] ) && ( $_SESSION [ 'GROUPS_ID' ] == 1 || 	$_SESSION [ 'GROUPS_ID' ] == 4 ) )  {
	$FC_SET [ 'SET_menu' ] 		= 'antropometrie|transitie|calculation'; 
	$FC_SET [ 'SET_function' ] 	= 'setupc|antropometrie|transitie|calculation';
}

// on the screen there may appear a module name to select.
$FC_SET [ 'SET_txt_menu' ] [ 'xsetupc' ]	= 'Instellingen';
$FC_SET [ 'SET_txt_menu' ] [ 'xdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vtransitie' ]	= 'Transitie calculatie';
$FC_SET [ 'SET_txt_menu' ] [ 'vantropometrie' ]	= 'BMI berekening';
$FC_SET [ 'SET_txt_menu' ] [ 'vcalculation' ]	= 'calculation';
$FC_SET [ 'SET_txt_menu' ] [ 'xtransitie' ]	= 'Transitie calculatie';
$FC_SET [ 'SET_txt_menu' ] [ 'xantropometrie' ]	= 'BMI berekening';
$FC_SET [ 'SET_txt_menu' ] [ 'xcalculation' ]	= 'voorbeeld';

Depending on the contents of the SET file modules are made available it may be needed to modify it to get the required functionality
  
### droplets related to functionality of this module 
no droplets

## The examle module: calculation
this consists out of the following php files:
- vcalculation.php.   The base module for the calculation for the front end. 
- xcalculation.php    The base module for the calculation for the backend end. The xcalculation.php differs from vcalculation.php in this phase only in the identification module
- vcalculation_check1.php   This module checks all the input parameters
- vcalculation_check2.php   This module makes all the calculations and the creates the screen output
- vcalculation_print1.php   This module generates the .pdf output
and the template
- cal_form.lte  Present in both NL as well as EN version. The template for the calculation function (both fro front end as well as backend) 
The output is created and kept for the duration of the session ( so a few hour upto one day ). 
The generated .pdf file may be available a longer period if the link is kept. 

## Error messages
 * When started for the first time an error message :*Oeps system not initialised and/or empty database* can be seen.
 The database tables are to be initialised:-> backend -> setupc
 The menusystem is to be installed: -> backend -> setup with parameter d_install detailed_