<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* function to remove old directories * /
function xrmdir( $dir )	{
	if ( is_dir( $dir ) )  {
 		$files = scandir( $dir );
  			foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) xrmdir( "$dir/$file" ); }
    		rmdir( $dir );
 	} else if ( file_exists( $dir ) ) { unlink( $dir );
}	}

/* functions to copy files and non-empty directories * /
function xcopy( $src, $dst ) {
// 	if ( file_exists( $dst ) ) { xrmdir( $dst ); }  // remove old directories if present disabled
 	if ( is_dir( $src ) )  {
    	$files = scandir( $src );
    	foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) xcopy( "$src/$file", "$dst/$file" ); }
  	} else if ( file_exists( $src ) ) { copy( $src, $dst );
}	}
 
/* create the empty directoriies * /
$out_file = LEPTON_PATH . MEDIA_DIRECTORY.'/archive';
if ( !file_exists( $out_file . "/" ) ) { 
	mkdir( $out_file, 0777 ); 
	copy( dirname(__FILE__).'/index.php', $out_file.'/index.php' );
}

$out_file = LEPTON_PATH.MEDIA_DIRECTORY.'/gsmoff';
if ( !file_exists( $out_file . "/" ) ) { 
	mkdir( $out_file, 0777 ); 
	copy( dirname(__FILE__).'/index.php', $out_file.'/index.php' );
}

$out_file = LEPTON_PATH.MEDIA_DIRECTORY.'/gsmoff/pdf';
if ( !file_exists( $out_file . "/" ) ) { 
	mkdir( $out_file, 0777 ); 
	copy( dirname(__FILE__).'/index.php', $out_file.'/index.php' );
}

/* install droplets  * /
$droplet_names = array();
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_castordoc.zip'))	$droplet_names [] = 'droplet_Gsm_castordoc';
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_mediadoc.zip'))	$droplet_names [] = 'droplet_Gsm_mediadoc';
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_mediadocIn.zip'))	$droplet_names [] = 'droplet_Gsm_mediadocIn';

if ( count ( $droplet_names ) >0 ) {
	LEPTON_handle::install_droplets ( 'gsmoffa', $droplet_names); //  dit verwijdert de droplets uit de install file !!
}

/* fill new directories * /
$place_in_delivery = dirname(__FILE__) . '/install';
$out_file = LEPTON_PATH.MEDIA_DIRECTORY.'/gsmoff/backuptest';

if ( !file_exists( $out_file . "/" ) ) { 
	mkdir( $out_file, 0777 ); 
	xcopy( $place_in_delivery, $out_file );
}

/* end install */

?>